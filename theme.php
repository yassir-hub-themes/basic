<?php
/**
 *sitemap
 *
 **/

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Tasawk\AppSettings\Setting\AppSettings;


$data = [
    'title' => 'Landing page',
    'icon' => 'icon-list3',
    'group' => 'landing-page',
    'descriptions' => 'Control landing page sections',
    'permission' => "manage_settings.landing_page",
    'sections' => [
        'general' => [
            'columns' => 2,
            'title' => 'About us',
            'icon' => 'icon-cog3',
            'inputs' => [
                [
                    'name' => 'html_block',
                    'type' => 'html',
                    "content" => view("ecommerce::settings.landing._about"),
                    "html_inputs" => [
                        [
                            'name' => 'about-title-ar',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'about-title-en',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'about-description-ar',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'about-description-en',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                    ],
                ],
                [
                    'name' => 'experience_years',
                    'type' => 'number',
                    'label' => 'Experience years',
                    'placeholder' => 'Experience years',
                    'rules' => 'nullable|string',
                ],
                [
                    'name' => 'available_products',
                    'type' => 'number',
                    'label' => 'Available products',
                    'placeholder' => 'Available products',
                    'rules' => 'nullable|string',
                ],
                [
                    'name' => 'happy_clients',
                    'type' => 'number',
                    'label' => 'Happy clients',
                    'placeholder' => 'Happy clients',
                    'rules' => 'nullable|string',
                ],
            ],
        ],
        'features' => [
            'columns' => 2,
            'title' => 'Features',
            'icon' => 'icon-cog3',
            'inputs' => [
                [
                    'name' => 'html_block',
                    'type' => 'html',
                    "content" => view("ecommerce::settings.landing._features"),
                    "html_inputs" => [
                        [
                            'name' => 'features-title-ar',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'features-title-en',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'features-description-ar',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'features-description-en',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                    ],
                ],
            ],
        ],
        'services' => [
            'columns' => 2,
            'title' => 'Services',
            'icon' => 'icon-cog3',
            'inputs' => [
                [
                    'name' => 'html_block',
                    'type' => 'html',
                    "content" => view("ecommerce::settings.landing._services"),
                    "html_inputs" => [
                        [
                            'name' => 'services-title-ar',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'services-title-en',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'services-description-ar',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'services-description-en',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                    ],
                ],
            ],
        ],
        'contact' => [
            'columns' => 2,
            'title' => 'Contact',
            'icon' => 'icon-cog3',
            'inputs' => [
                [
                    'name' => 'html_block',
                    'type' => 'html',
                    "content" => view("ecommerce::settings.landing._contact"),
                    "html_inputs" => [
                        [
                            'name' => 'contact-title-ar',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'contact-title-en',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'contact-description-ar',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'contact-description-en',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                    ],
                ],
            ],
        ],
        'image' => [
            'columns' => 2,
            'title' => 'Images',
            'icon' => 'icon-cog3',
            'inputs' => [
                [
                    'name' => 'about-image',
                    'type' => 'image',
                    'label' => 'About',
                    "class" => "file-input",
                    'rules' => 'nullable|file',
                ],
                [
                    'name' => 'contact-image',
                    'type' => 'image',
                    'label' => 'Contact',
                    "class" => "file-input",
                    'rules' => 'nullable|file',
                ],

            ],
        ],
    ],
];
$connection = AdminBase::buildDBconnection();
if (Schema::connection($connection)->hasTable('sliders')) {
    $position = DB::connection(AdminBase::buildDBconnection())
        ->table('slider_positions as sp')
        ->select(['sp.id', 'spt.title'])
        ->join('slider_positions_translations as spt', 'sp.id', '=', 'spt.position_id')
        ->where('spt.locale', app()->getLocale())
        ->get()->pluck('title', 'id');
    $data['sections']['slider_position'] = [
        'columns' => 2,
        'title' => 'Slider',
        'icon' => 'icon-cog3',
        'inputs' => [
            [
                'type' => 'select',
                'name' => 'slider-position',
                'label' => 'Slider position',
                'placeholder' => __("Select"),
                'options' => $position
            ]
        ]
    ];
}
AppSettings::addPage('landing-page', $data);


