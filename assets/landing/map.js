$(window).on("load", function () {
    let locations = $("#map").data("locations");
    let infowindow = new google.maps.InfoWindow();
    let activeInfoWindow;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: new google.maps.LatLng(24.774265, 46.738586),
    });

    for (let i = 0; i < locations.length; i++) {
        const location = locations[i];
        const contentString =
            ` <div class="branch" style="width: auto">
           <div class="branch-img">
            <img src="${location.image}" class="img-fluid">
            </div>
            <div class="branch-info">
            <h4 class="branch-name"> ${location.name}
            </h4>
            <a href="tel:${location.phone}" class="branch-contact">
            <i class="fas fa-phone"></i> ${location.phone}
            </a>
            <a href="mailto:${location.email}" class="branch-contact">
            <i class="far fa-envelope"></i>  ${location.email}
            </a>
            </div>
                        </div>`;
        const infowindow = new google.maps.InfoWindow({
            content: contentString,
        });

        const marker = new google.maps.Marker({
            position: new google.maps.LatLng(location.map_location['lat'], location.map_location['lng']),
            map,
            icon: theme_assets_url + "landing/images/pin.png"
        });
        marker.addListener("click", () => {
            if (activeInfoWindow) {
                activeInfoWindow.close();
            }
            infowindow.open(map, marker);
            activeInfoWindow = infowindow;
        });
    }

});


