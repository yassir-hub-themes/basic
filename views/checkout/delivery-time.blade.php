<div class="delivery-time-content">
    {{html()->element('h2')->class('page-title')->html(__('Select delivery date'))}}
    <div class="delivery-time">
        <input type="hidden" id="delivery_date" value="{{old('delivery_date',date('Y-m-d'))}}" name="delivery_date">
        <input type="hidden" id="delivery_time" value="{{old('delivery_time',date('H:i'))}}" name="delivery_time">
        {{html()->hidden('shift-id')}}
        <label class="delivery-item">
            {{html()->radio('delivery_type')->value('immediately')->checked(old('delivery_type') == 'immediately')}}
            <div class="delivery-item-text">
                <div class="text">
                    <i class="fas fa-stopwatch"></i>
                    @lang('Immediately')
                </div>
            </div>
        </label>
        @if(!empty($shifts))
            <label class="delivery-item">
                <input type="radio" name="delivery_type" value="selected-date" @if(old('delivery_type') == 'selected-date') checked @endif class="select-date">
                <div class="delivery-item-text">
                    <div class="text">
                        <i class="fas fa-clock"></i>
                        @lang('Select date')
                    </div>
                </div>
            </label>
        @endif
    </div>
    @error('delivery_type')
    <p class="text-center">
        <span class="error-text-alert">
            {{ $message }}
        </span>
    </p>
    @enderror
    @error('delivery_date')
    <p class="text-center">
        <span class="error-text-alert">
            {{ $message }}
        </span>
    </p>
    @enderror
    @error('delivery_time')
    <p class="text-center">
        <span class="error-text-alert">
            {{ $message }}
        </span>
    </p>
    @enderror
</div>
@push("styles")
    <style>
        .time-range {
            width: initial;
        }

        .time-range .time-text {
            padding-top: 5px;
            width: 50px;
            height: 50px;
            border-radius: 50%;
        }

        .times-list {
            max-width: initial;
        }

        .delivery-date-input {
            border: none;
            border-bottom: 1px solid #dbdbdb;
            padding: 0 20px;
            height: 50px;
            font-size: 18px;
            color: #a4a4a4;
            width: 100%;
            -webkit-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            background-color: #fff;
        }
    </style>
@endpush
@push('modals')
    @if(!empty($shifts))
        <div tabindex="-1" class="modal fade date-modal" id="date-modal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('Choose delivery day')</h5>
                        <div class="date-picker-container">
                            <input type="date" id="delivery_date_select" class="delivery-date-input"
                                   value="{{old('delivery_date',date('Y-m-d'))}}"
                                   data-default-date="{{old('delivery_date',date('Y-m-d'))}}">
                            <span class="fas fa-calendar-alt"></span>
                        </div>
                    </div>
                    <div class="modal-body">
                        <h5 class="modal-title pb-3">@lang('Select a period')</h5>
                        <div class="times-list text-center">
                            @foreach ($shifts as $_shift)
                                @php($delivery_value = $_shift['from'].' - '.$_shift['to'])
                                <div class="pb-2">
                                    <div>@lang('From :FROM to :TO',['FROM'=>$_shift['from'],'TO'=>$_shift['to']])</div>
                                    <div class="hours">
                                        @foreach($_shift['hours'] as $hour)
                                            <label class="time-range">
                                                <input type="radio"
                                                       data-shift-id="{{$_shift['id']}}"
                                                       @if(old('delivery_time') == $hour) checked @endif
                                                       class="delivery_time_select" value="{{$hour}}"
                                                       name="time-range">
                                                <span class="time-text">{{$hour}}</span>
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a role="button" data-dismiss="modal" class="continue-btn select-delivery">
                            @lang('Continue')
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endpush
@push('scripts')
    <script>
        ;(function (window, document, $) {
            if ($(window).width() > 1199) {
                let opening_days = @json($opening_days);
                let defs = @json($defs);

                function disabled_days(date) {
                    return !Object.values(opening_days).includes(defs['_' + date.getDay()]);
                }

                $(".delivery-date-input").flatpickr({
                    locale: '{{app()->getLocale()}}',
                    minDate: new Date({{$firstAvailableInWeek}}),
                    dateFormat: "d M Y",
                    disable: [disabled_days],
                    defaultDate: @if(old('delivery_date')) '{{date('d M Y',strtotime(old('delivery_date')))}}' @else new Date({{$firstAvailableInWeek}}) @endif,
                    onChange: function (selectedDates, dateStr, instance) {
                        $('#delivery_date').val(flatpickr.formatDate(selectedDates[0], "Y-m-d"));
                    },
                });
            }
            $('.select-delivery').on('click', function () {
                let time = $("input[name='time-range']:checked");
                $('#delivery_time').val(time.val());
                $('#shift-id').val(time.data('shift-id'));
            });
            $('#immediately').click(function (e) {
                let now = new Date();
                $('#delivery_time').val(now.getHours() + ':' + now.getMinutes())
            });

        })(window, document, jQuery)
    </script>
@endpush
