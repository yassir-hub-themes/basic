<div class="modal fade addressBook-modal" id="addressBook-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="address-list">
                    @foreach(Ecommerce::auth()->user()->addresses ?? [] as $address)
                        <label class="adress-item">
                            <input type="radio" data-info="{{ json_encode($address->map_location) }}"
                                   id="change-address" name="adressBook" value="{{ $address->id }}">
                            <div class="adress-head">
                                    <span class="name">
                                        {{ $address->name }}
                                    </span>
                                <span class="checkmark">
                                        <i class="fas fa-check-circle"></i>
                                    </span>
                            </div>
                            <p class="address-text">
                                {{ $address->map_location['address'] }}
                            </p>
                            <strong class="address-phone">
                                {{ $address->phone }}
                            </strong>
                        </label>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <a role="button" data-dismiss="modal" class="continue-btn">
                    @lang('Continue')
                </a>
            </div>
        </div>
    </div>
</div>
