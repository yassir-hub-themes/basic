<h2 class="page-title">
    @lang('Select shipping address')
</h2>
<div class="row">
    <div class="col-md-6">
        <div class="float-field">
            @error('country_id')
            <span class="error-text-alert">
                {{ $message }}
            </span>
            @enderror
            <select class="new-address-select country-select" name="country_id" data-placeholder="@lang("Select Country")">
                @foreach ($countries as $country)
                    <option value=""></option>
                    <option
                        data-lat="{{$country->latitude}}"
                        data-lng="{{$country->longitude}}"
                        @if(old("country_id") == $country->id) selected
                        @endif value="{{ $country->id }}">{{ $country->name }}</option>
                @endforeach
            </select>
            <label>@lang('Country')</label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="float-field">
            @error('city_id')
            <span class="error-text-alert">
               {{ $message }}
            </span>
            @enderror
            <select class="new-address-select city-select" disabled name="city_id">
                <option>@lang("Choose country first")</option>
            </select>
            <label>@lang('City')</label>
        </div>
    </div>
</div>
<div class="checkout-map">
    <p style="margin: 0;color:var(--danger)"><i class="fas fa-exclamation-triangle"></i> @lang("Please enable automatically location or select on the map")</p>
    <div id="checkout-map"></div>
    <a role="button" class="locationButton @if(!old('address.address_name')) shake shake-once @endif"
       data-toggle="tooltip" data-placement="left" title="@lang('Click to find your location automatic')">
        <img src="{{ Ecommerce::theme()->asset('shop/images/checkout/location.png') }}">
    </a>
    <div class="locationInput-container" style="left: 35px;right: 70px">
        <input type="text" class="locationInput" readonly value="{{old('address.address_name')}}">
        @auth(Ecommerce::auth()->guard())
            <a role="button" class="adressBook" data-toggle="modal" data-target="#addressBook-modal">
                <i class="fas fa-address-book"></i>
            </a>
        @endauth
    </div>
    <?php
    $address_lat = old('address.lat', '24.774255');
    $address_lang = old('address.lang', '46.737586534');
    ?>
    <input type="hidden" name="address_id" id="address_id" value="{{old('address_id')}}">
    <input type="hidden" name="address[lat]" id="map-lat" value="{{$address_lat}}">
    <input type="hidden" name="address[lang]" id="map-lng" value="{{$address_lang}}">
    <input type="hidden" name="address[address_name]" id="locationInput" value="{{old('address.address_name')}}">
</div>

@push('styles')
    <style>
        .shake {
            animation: shake .82s cubic-bezier(.36, .07, .19, .97) both infinite;
            transform: translate3d(0, 0, 0);
            backface-visibility: hidden;
            perspective: 1000px
        }

        @keyframes shake {
            10%, 90% {
                transform: translate3d(-1px, 0, 0)
            }
            20%, 80% {
                transform: translate3d(2px, 0, 0)
            }
            30%, 50%, 70% {
                transform: translate3d(-4px, 0, 0)
            }
            40%, 60% {
                transform: translate3d(4px, 0, 0)
            }
        }
    </style>
@endpush
@push('scripts')
    {{Utilities::MapScriptTag()}}
    <script>
        ;(function () {

            function initMap() {
                const locationButton = document.getElementsByClassName('locationButton')[0];
                locationButton.addEventListener("click", () => {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                            (position) => {
                                const pos = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude,
                                };
                                marker.setPosition(pos);
                                map.setCenter(pos);
                                handleMarkerOutsideCountry(pos.lat, pos.lng);
                                updateInput(pos)
                            },
                        );
                    }
                });
                google.maps.event.addListener(marker, "dragend", function (e) {
                    handleMarkerOutsideCountry(e.latLng.lat(), e.latLng.lng());
                    updateInput(e.latLng);
                });
                google.maps.event.addListener(map, "click", function (e) {
                    const pos = {
                        lat: e.latLng.lat(),
                        lng: e.latLng.lng(),
                    };
                    marker.setPosition(pos);
                    handleMarkerOutsideCountry(e.latLng.lat(), e.latLng.lng());
                    updateInput(e.latLng);
                });
            }

            function updateInput(latLng) {
                let geocoder = new google.maps.Geocoder();
                $('#map-lat').val(latLng.lat);
                $('#map-lng').val(latLng.lng);
                geocoder.geocode({
                    'latLng': latLng
                }, function (results) {
                    $('#locationInput').val(results[0].formatted_address);
                    document.getElementsByClassName('locationInput')[0].value = results[0].formatted_address
                });
            }

            function setMarkerOnMap(lat, lng, map, zoom = 5) {
                if (!(lat && lng)) {
                    return;
                }
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    map: map,
                    draggable: true,
                    icon: "{{Ecommerce::theme()->asset('shop/images/pin.png')}}"
                });
                google.maps.event.addListener(marker, "dragend", function (e) {
                    errors = 0;
                    updateInput(e.latLng);
                    handleMarkerOutsideCountry(e.latLng.lat(), e.latLng.lng());
                });
                markers.push(marker);
                map.setCenter(new google.maps.LatLng(lat, lng));
                map.setZoom(zoom);
                map.panTo(marker.position);
                updateInput(marker.position);
            }

            function handleMarkerOutsideCountry(lat, lng) {
                $(".map-error-msg").html("");
                let api_key = "{{settings()->group('third-party')->get('google_map_key', env("GOOGLE_MAP_KEY"))}}";
                let selectedCountry = $(".city-select").find("option:selected");
                selectedCountry = selectedCountry.val() !== "" ? selectedCountry.text() : null;
                if (!selectedCountry) {
                    return;
                }
                fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${api_key}&language={{app()->getLocale()}}`)
                    .then(response => response.json())
                    .then(data => {
                        let country = data.results[0].address_components.filter(function (location) {
                            return location.types.includes("country");
                        })[0];
                        if (typeof country == "undefined") {
                            $(".map-error-msg").text("@lang('Please choose a more accurate area')");
                            errors++;
                        } else if (selectedCountry !== country.long_name) {
                            $(".map-error-msg").text("@lang('Sorry this specified area out of the zone')");
                            errors++;
                        }
                        // let country_long_name_val = $(`.country-select option:contains(${country.long_name})`).val();
                        // if (country_long_name_val != $('.country-select').val()) {
                        //     $('.country-select').val(country_long_name_val).change()
                        // }

                    });
            }

            function restMap() {
                for (let i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
            }

            let errors = 0;
            let markers = [];
            $('.shake-once').hover(function () {
                $(this).removeClass('shake shake-once')
            });
            const myLatLng = {
                lat: {{$address_lat}},
                lng: {{$address_lang}}
            };
            $(document).ready(function () {
                initMap();
            });
            let map = new google.maps.Map(document.getElementById("checkout-map"), {
                center: myLatLng,
                zoom: 17,
            });
            let marker = new google.maps.Marker({
                position: map.getCenter(),
                draggable: true,
                map,
                icon: "{{Ecommerce::theme()->asset('shop/images/pin.png')}}"
            });
            markers.push(marker);
            $('#change-address').on('click', function () {
                $('#address_id').val($(this).val());
                let info = JSON.parse($(this).attr('data-info'));
                $('#map-lat').val(parseFloat(info.lat));
                $('#map-lng').val(parseFloat(info.lng));
                $('#locationInput').val(info.address);
                const pos = {
                    lat: parseFloat(info.lat),
                    lng: parseFloat(info.lng),
                };
                marker.setPosition(pos);
                map.setCenter(pos);
            });
            $(".country-select").on("change", function () {
                let country = $(this).val();
                let select = $(".city-select");
                let lat = $(this).find("option:selected").data("lat");
                let lng = $(this).find("option:selected").data("lng");
                select.empty().attr("disabled", "disabled");
                $.ajax({
                    url: route("ajax.countries.country.cities", country),
                    success: function (data) {
                        select.html(data).removeAttr("disabled");
                        let oldCityId;
                        @if(old("city_id"))
                            oldCityId = select.find('[value="{{old("city_id")}}"]');
                        oldCityId.attr('selected', 'selected');
                        lat = oldCityId.data("lat");
                        lng = oldCityId.data("lng");
                        @endif
                        @if(!old("city_id"))
                        restMap();
                        setMarkerOnMap(lat, lng, map, 10);
                        @endif
                    }
                });
            });
            $(".city-select").on("change", function () {
                let lat = $(this).find("option:selected").data("lat");
                let lng = $(this).find("option:selected").data("lng");
                @if(!old("city_id"))
                restMap();
                setMarkerOnMap(lat, lng, map, 10);
                @endif
            });
            @if(old("country_id"))
            $(".country-select").trigger("change");
            @endif
        })();
    </script>
@endpush
