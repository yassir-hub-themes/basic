<?php
$payments = settings()->group("ecommerce")->json("allowed_payment_methods");
if(empty($payments)){
    return;
}
?>
<div class="payment-methods-content m-5">
    <h2 class="page-title">
        @lang('Select payment method')
    </h2>
    <div class="payment-methods">
        @foreach ($payments as $payment)
            <label class="payment-method" data-toggle="tooltip" data-placement="top"
                   title="@lang(Str::replace("_"," ",$payment))">
                <input type="radio" name="payment_method" @if(old('payment_method') == $payment) checked @endif value="{{ $payment }}">
                <div class="payment-img loading-img">
                    <img class="lazy-img"
                         data-src="{{ upload_storage_url(Ecommerce::settings()->get("$payment-image")) }}">
                </div>
            </label>
        @endforeach
    </div>
    @error('payment_method')
    <p class="text-center">
        <span class="error-text-alert">{{ $message }}</span>
    </p>
    @enderror
</div>
