<option></option>
@foreach($lists as $list)
    <option value="{{$list->id}}"
            data-has-boundaries="{{(is_null($list->boundaries) || empty(json_decode($list->boundaries,true)))?0:1  }}"
            data-boundaries="{{$list->boundaries}}"
            data-lat="{{$list->latitude}}"
            data-lng="{{$list->longitude}}"
            @if($loop->first)selected @endif
    >{{$list->name}}</option>
@endforeach
