<div class="form-group">
    <div class="input-container">
        <input type="email" class="form-control login_number" placeholder="@lang('Email')">
    </div>
    <span class="display_error ajax_error_object error-text-alert"></span>
</div>
@if (Ecommerce::settings()->get("ecommerce-login-by") == "email-and-password")
    <div class="form-group">
        <div class="input-container">
            <input type="password" class="form-control login_password" placeholder="********">
        </div>
        <span class="display_error ajax_error_password error-text-alert"></span>
    </div>
@endif
