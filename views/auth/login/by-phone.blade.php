<div class="form-group">
    <div class="input-container">
        <input type="number" class="form-control tel-input login_number"
               autocomplete="new-password"
               placeholder="5xxxxxxxx">
        <span class="input-flag">
            <img src="{{Ecommerce::theme()->asset('shop/images/flag.png')}}" class="img-fluid"> +966
        </span>
    </div>
    <span class="display_error ajax_error_object error-text-alert"></span>
</div>
@if (Ecommerce::settings()->get("ecommerce-login-by") == "phone-and-password")
    <div class="form-group">
        <div class="input-container">
            <input type="password" class="form-control login_password"
                   autocomplete="new-password"
                   placeholder="********">
        </div>
        <span class="display_error ajax_error_password error-text-alert"></span>
    </div>
@endif
