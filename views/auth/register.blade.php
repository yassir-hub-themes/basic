@extends("theme::layouts.master")
@section('page_title',__('Register'))
@section("content")
    <section class="content-section register-content">
        <div class="container">
            <h2 class="page-title">
                @lang('Register')
            </h2>
            <form class="register-form" action="{{ route('register.check') }}" method="POST">
                @csrf
                <div class="form-group phone required">
                    <label>
                        @lang('Phone')
                    </label>
                    <div class="input-container">
                        <input tabindex="1" type="tel" name="phone" value="{{ old('phone','') }}"
                               class="form-control"
                               id="phone"
                               autocomplete="new-password">
                        <input type="hidden" name="full-phone" id="full-phone" value="{{ old('full-phone','') }}">
                        <input type="hidden" name="country-code" id="country-code" value="{{old('country-code',tenant()->account->country)}}">
                    </div>
                    @error('phone')
                    <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group name required">
                    <label>
                        @lang('Full name')
                    </label>
                    <input type="text" value="{{ old('name') }}" name="name" class="form-control" autocomplete="new-password">
                    @error('name')
                    <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group email">
                    <label>
                        @lang('Email')
                    </label>
                    <input type="email" value="{{ old('email') }}" name="email" class="form-control" autocomplete="off">
                    @error('email')
                    <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group password">
                    <label>
                        @lang('Password')
                    </label>
                    <input type="password" name="password" class="form-control" autocomplete="new-password">
                    @error('password')
                    <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
                    @enderror
                </div>
                @if(Ecommerce::settings()->get('ecommerce-page-terms-of-service'))
                    <div class="form-group required terms">
                        <div class="agree">
                            <label class="custom-checkbox">
                                <input type="checkbox" name="terms" id="terms-check" @if(old('terms')) checked @endif>
                                <span class="check-mark"><i class="fas fa-check"></i></span>
                            </label>
                            <span class="text">
                            <span onclick="document.getElementById('terms-check').click()">@lang('I agree to')</span>
                                <a href="{{route("page.show",Ecommerce::settings()->get('ecommerce-page-terms-of-service'))}}"
                                    target="_blank"
                                >
                                    @lang('Terms and conditions')
                                </a>
                            </span>
                        </div>
                        @error('terms')
                        <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
                        @enderror
                    </div>
                @endif
                <button type="submit" class="submit-btn">
                    @lang('Submit')
                </button>
            </form>
        </div>
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/css/intlTelInput.css" integrity="sha512-gxWow8Mo6q6pLa1XH/CcH8JyiSDEtiwJV78E+D+QP0EVasFs8wKXq16G8CLD4CJ2SnonHr4Lm/yY2fSI2+cbmw==" crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <style>
        #phone,
        .iti {
            direction: ltr;
            text-align: left;
        }

        .iti--allow-dropdown .iti__flag-container .iti__selected-flag,
        .iti--allow-dropdown .iti__flag-container:hover .iti__selected-flag {
            background-color: transparent;
        }

        .iti.iti--allow-dropdown {
            width: 100%;
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/js/intlTelInput-jquery.js"
            integrity="sha512-xo8nGg61671g6gPcRbOfQnoL+EP5SofzlUHdZ/ciHev4ZU/yeRFf+TM5dhBnv/fl05vveHNmqr+PFtIbPFQ6jw=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"></script>
    <!--suppress JSUnresolvedFunction -->
    <script>
        ;(function () {
            <?php
            $countries = Tasawk\Locations\Models\Country::enabled()->get();
            ?>
            $(document).ready(function () {
                $("#user_check input").click(function () {
                    $('.password-info').css('display', $(this).val().trim() == 'guest' ? 'none' : 'block')
                });
                $("#phone").intlTelInput({
                    separateDialCode: true,
                    initialCountry: '{{old('country-code',)}}',
                    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/js/utils.min.js",
                    onlyCountries: @json($countries->isEmpty() ? [tenant()->account->country] : $countries->pluck('iso2')->toArray()),
                });
                $("#phone").on('countrychange', function () {
                    $("#country-code").val($("#phone").intlTelInput("getSelectedCountryData").iso2);
                    $("#full-phone").val($("#phone").intlTelInput("getNumber"));
                });
                $("#phone").on('change load', function () {
                    $("#country-code").val($("#phone").intlTelInput("getSelectedCountryData").iso2);
                    $("#full-phone").val($("#phone").intlTelInput("getNumber"));
                });
            });
        })();
    </script>
@endpush
