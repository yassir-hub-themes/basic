@extends("theme::layouts.master")
@section('page_title',__('Forget password'))
@section("content")
    <!-- Start Page Content -->
    <section class="content-section register-content">
        <div class="container">
            <h2 class="page-title">
                @lang('Forget password')
            </h2>
            <form class="register-form" action="{{ route('forget-password.check') }}" method="POST">
                @csrf
                

                @if (in_array(Ecommerce::settings()->get("ecommerce-login-by"),["phone-and-password","phone-only"]))

                    <div class="form-group phone required">
                        <label>
                            @lang('Phone')
                        </label>
                        <div class="input-container">
                            <input tabindex="1" type="phone" name="object" value="{{ old('object') }}"
                                autocomplete="off"
                                class="form-control tel-input"
                                placeholder="5xxxxxxxx">
                            <span class="input-flag">
                                <img src="{{Ecommerce::theme()->asset('shop/images/flag.png')}}" class="img-fluid">+966
                            </span>
                        </div>
                        @error('object')
                            <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
                        @enderror
                    </div>

                @else

                    <div class="form-group email">
                        <label>
                            @lang('Email')
                        </label>
                        <input type="email" value="{{ old('object') }}" name="object" class="form-control" autocomplete="off">
                        @error('object')
                            <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
                        @enderror
                    </div>

                @endif

                <button type="submit" class="submit-btn">
                    @lang('Submit')
                </button>
            </form>
        </div>
    </section>
    <!-- End Page Content -->
@endsection