@extends("theme::layouts.master")
@section('page_title',__('Bank accounts'))
@section("content")


<!-- Start Page Content -->
<section class="content-section banks-content">
    <div class="container">
        <h2 class="page-title">
            @lang('Bank accounts')
        </h2>
        <div class="banks-list">
            @foreach ($banks as $bank)
                <div class="bank">
                    <div class="loading-img bank-img">
                        <img class="lazy-img" alt="{{ $bank->account_name }}" title="{{ $bank->account_name }}" data-src="{{upload_storage_url($bank->account_image)}}">
                    </div>
                    <ul class="bank-info list-unstyled">
                        <li>
                            <span>
                                @lang('Account name')
                            </span>
                            {{ $bank->account_name }}
                        </li>
                        <li>
                            <span>
                                @lang('Account number')
                            </span>
                            {{ $bank->account_number }}
                        </li>
                        <li>
                            <span>
                                @lang('Account IBAN')
                            </span>
                            {{ $bank->account_iban }}
                        </li>
                    </ul>
                </div>
            @endforeach

        </div>
    </div>
</section>
<!-- End Page Content -->


@endsection