<?php

/** @var \Tasawk\Items\Model\Items $product */
$hasSliders = isset($sliders) && count($sliders);
$hasServices = isset($services) && count($services);
$hasProducts = isset($products) && count($products)
?>
@extends("theme::layouts.master")
@section('page_title',__('Home'))
@section("content")
    @if ($hasSliders)
        <main class="main-slider-section">
            <div class="main-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach ($sliders as $slider)
                            <div class="swiper-slide">
                                <a href="{{ $slider->link }}" class="main-slide">
                                    <div class="loading-img slider-img">
                                        <img class="lazy-img" data-src="{{upload_storage_url($slider->image)}}"
                                             title="{{$slider->title}}" alt="{{$slider->title}}">
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-btn-next swiper-btn"><span class='fas fa-chevron-left'></span></div>
                <div class="swiper-btn-prev swiper-btn"><span class='fas fa-chevron-right'></span></div>
            </div>
        </main>
    @endif
    @if ($hasServices)
        <section class="slider-section services-section @if(!$hasSliders) m-4 @endif">
            <div class="container">
                <div class="services-content">
                    <div class="section-head">
                        <h3 class="section-title">
                            @lang('Services')
                        </h3>
                        <a href="{{route("shop.type",'services')}}" class="section-more">
                            @lang('Show all')
                        </a>
                    </div>
                    <div class="services-slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                @foreach ($services as $item)
                                    <div class="swiper-slide">
                                        @include('theme::items.item')

                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="swiper-btn-next swiper-btn"><span class='fas fa-chevron-left'></span></div>
                        <div class="swiper-btn-prev swiper-btn"><span class='fas fa-chevron-right'></span></div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if(settings()->group("ecommerce")->bool("services_banner_enabled",true))
        <section class="banner-section">
            <div class="banner-content"
                 style="background-image: url({{Ecommerce::theme()->asset('shop/images/banner/bg.jpg')}});">
                <div class="banner-text">
                    <div class="container">
                        <div class="banner-text-content">
                            <h2 class="banner-subtitle">
                                @lang("Professional maintenance services")
                            </h2>
                            <h1 class="banner-title">
                                @lang("For all types of filters in Riyadh")
                            </h1>
                            <a href="{{route("shop.type",'services')}}" class="banner-btn">@lang("Order now")</a>
                        </div>
                    </div>
                </div>
                <div class="banner-shape">
                    <img src="{{Ecommerce::theme()->asset('shop/images/banner/ico.png')}}">
                </div>
            </div>
        </section>
    @endif
    @if ($hasProducts)
        <section class="slider-section products-section">
            <div class="container">
                <div class="products-content">
                    <div class="section-head">
                        <h3 class="section-title">
                            @lang('Products')
                        </h3>
                        <a href="{{route("shop.type",'products')}}" class="section-more">
                            @lang('Show all')
                        </a>
                    </div>
                    <div class="products-slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                @foreach ($products as $item)
                                    <div class="swiper-slide">
                                        @include('theme::items.item')
                                    </div>
                                @endforeach

                            </div>
                        </div>
                        <div class="swiper-btn-next swiper-btn"><span class='fas fa-chevron-left'></span></div>
                        <div class="swiper-btn-prev swiper-btn"><span class='fas fa-chevron-right'></span></div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection

