@php
    use Tasawk\Items\Model\Items;
    $about_page_id =Ecommerce::settings()->get('ecommerce-page-about');
    $term_of_services_page_id = Ecommerce::settings()->get('ecommerce-page-terms-of-service');
    $privacy_policy_page_id = Ecommerce::settings()->get('ecommerce-page-privacy-policy');
    $pages_ids = array_unique([$about_page_id,$term_of_services_page_id,$privacy_policy_page_id]);
    $pages = \Tasawk\Cms\Model\Cms::find($pages_ids)->pluck("page_slug","id");
    $services_count = Items::where("type", 'service')->count();
@endphp
        <!DOCTYPE HTML>
<HTML lang="{{app()->getLocale()}}" dir="{{app()->getLocale() == 'ar' ? 'rtl':'ltr' }}">
<head>
    @include("theme::layouts.inc.head")
    <script>
        let base_url = "{{url("/")}}";
    </script>
    @routes
</head>
<body class="@filter('body_class',' ' . request()->route()->getName() . ' ' )">
@include("theme::layouts.inc.header")
@yield("content")
@include("theme::layouts.inc.footer")
{!!Ecommerce::settings()->get('seo-footer-scripts')!!}
</body>
</HTML>
