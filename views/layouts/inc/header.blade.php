<header>
    <div class="container">
        <div class="header">
            <div class="logo-bars">
                <a role="button" class="menu-btn">
                    <i class="fas fa-bars"></i>
                </a>
                <a href="{{ route('home') }}" class="logo">
                    @if(Ecommerce::settings()->get("ecommerce_site_logo"))
                        <img src="{{upload_storage_url(Ecommerce::settings()->get("ecommerce_site_logo"))}}"
                             class="img-fluid" alt="@lang('logo')" title="@lang('logo')">
                    @elseif(settings()->group('default')->get('logo'))
                        <img src="{{ upload_storage_url(settings()->group('default')->get('logo')) }}" class="img-fluid"
                             alt="@lang('logo')" title="@lang('logo')">
                    @else
                        <img src="{{AdminBase::theTheme()->asset('app-assets/images/svg/logo-color.svg') }}"
                             class="img-fluid" alt="@lang('logo')" title="@lang('logo')">
                    @endif
                </a>
            </div>
            <div class="menu-overlay"></div>
            <nav>
                <a role="button" class="menu-close">
                    <i class="fas fa-times"></i>
                </a>
                <ul class="mo-navbar list-unstyled">
                    <li class="mo-nav-item">
                        <a href="{{ route('home') }}" class="mo-nav-link">
                            @lang('Home')
                        </a>
                    </li>
                    <li class="mo-nav-item">
                        <a href="{{route("shop.type","products")}}" class="mo-nav-link">
                            @lang('Products')
                        </a>
                    </li>
                    @if($services_count)
                        <li class="mo-nav-item">
                            <a href="{{route("shop.type","services")}}" class="mo-nav-link">
                                @lang('Services')
                            </a>
                        </li>
                    @endif
                    @if(Arr::exists($pages,$about_page_id) && $slug = Arr::get($pages,$about_page_id) )
                        <li class="mo-nav-item">
                            <a href="{{route("page.show",$slug)}}"
                               class="mo-nav-link">
                                @lang('About us')
                            </a>
                        </li>
                    @endif
                    <li class="mo-nav-item">
                        <a href="{{ route('contact-us') }}" class="mo-nav-link">
                            @lang('Contact us')
                        </a>
                    </li>
                </ul>
                <?php $convertLang = (\App::getLocale() == "en") ? "ar" : "en"; ?>
                <a hreflang="{{ $convertLang }}"
                   href="{{ LaravelLocalization::getLocalizedURL($convertLang, null, [], true) }}"
                   data-language="{{ $convertLang }}" class="lang">
                    {{ $convertLang }}
                </a>
            </nav>
            <div class="header-tools">
                @include("theme::partials.navbar-currencies")
                @if (Ecommerce::auth()->check())
                    <a role="button" class="notification-icon header-icon">
                        <i class="fas fa-bell"></i>
                    </a>
                    <div class="user-cont">
                        <a role="button" class="user-icon header-icon">
                            <i class="fas fa-user"></i>
                        </a>
                        <div class="drop-down">
                            <a href="{{ route('profile.index') }}">
                                @lang("My account")
                            </a>
                            <a href="{{ route('addresses.index') }}">
                                @lang('Address book')
                            </a>
                            <a href="{{ route('orders.index') }}">
                                @lang('Orders')
                            </a>
                            <a href="{{ route('ecommerce.logout') }}">
                                @lang('Logout')
                            </a>
                        </div>
                    </div>
                @else
                    <a role="button" class="logform-link" data-toggle="modal" data-target="#login-modal">
                        @lang('Login')
                    </a>
                    <a href="{{ route('register') }}" class="reg-link">
                        @lang('Register')
                    </a>
                    <a role="button" class="modal-link header-icon" data-toggle="modal" data-target="#login-modal">
                        <i class="fas fa-user"></i>
                    </a>
                @endif
                <a href="{{route("cart")}}" class="cart-link header-icon">
                    <i class="fas fa-shopping-basket"></i>
                    <strong class="cart-items cart-items-count">{{\Cart::getTotalQuantity()}}</strong>
                </a>
            </div>
        </div>
    </div>
</header>
@if(Route::currentRouteName() != 'home')
    <section class="breadcrumb-section">
        <div class="container">
            <ul class="breadcrumb-ul">
                <li class="breadcrumb-li">
                    <a href="{{ route('home') }}" class="breadcrumb-a">
                        @lang('Home')
                    </a>
                </li>
                @foreach ($breadcrumbs as $breadcrumb)
                    <li class="breadcrumb-li">
                        <a href="{{ $breadcrumb['route'] }}" class="breadcrumb-a">
                            {{ $breadcrumb['title'] }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
@endif
