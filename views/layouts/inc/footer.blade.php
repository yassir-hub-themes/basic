<footer>
    <div class="container">
        <div class="footer">
            <div class="row">
                <div class="col-lg-5 col-md-8">
                    <div class="footer-about">
                        <div class="footer-about-logo">
                            @if(Ecommerce::settings()->get("ecommerce_site_logo"))
                                <img src="{{upload_storage_url(Ecommerce::settings()->get("ecommerce_site_logo"))}}"
                                     class="img-fluid" alt="@lang('logo')" title="@lang('logo')">
                            @elseif(settings()->group('default')->get('logo'))
                                <img src="{{ upload_storage_url(settings()->group('default')->get('logo')) }}"
                                     class="img-fluid" alt="@lang('logo')" title="@lang('logo')">
                            @else
                                <img src="{{AdminBase::theTheme()->asset('app-assets/images/svg/logo-color.svg') }}"
                                     class="img-fluid" alt="@lang('logo')" title="@lang('logo')">
                            @endif
                        </div>
                        <p class="footer-about-text">
                            @php $lang=app()->getLocale();  @endphp
                            {{Ecommerce::settings()->get("ecommerce-$lang-description")}}
                        </p>
                        <div class="socials">
                            @foreach(array_filter(settings()->json("social_links"))??[] as $key => $social )
                                <a href="{{$social['link']}}" class="social">
                                    <i class="{{$social['icon']}}"></i>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="footer-list-content">
                        <h3 class="footer-head footer-list-head">
                            @lang('Important links')
                        </h3>
                        <ul class="footer-list list-unstyled">
                            @if(Arr::exists($pages,$about_page_id) && $slug = Arr::get($pages,$about_page_id))
                                <li class="list-item">
                                    <a
                                        href="{{route("page.show",$slug)}}"
                                        class="list-link">
                                        @lang('About us')
                                    </a>
                                </li>
                            @endif
                            <li class="list-item">
                                <a href="{{ route('bank-accounts') }}" class="list-link">
                                    @lang('Bank accounts')
                                </a>
                            </li>
                            @if(Arr::exists($pages,$term_of_services_page_id) && $slug = Arr::get($pages,$term_of_services_page_id))
                                <li class="list-item">
                                    <a
                                        href="{{route("page.show",$slug)}}"
                                        class="list-link">
                                        @lang('Terms and conditions')
                                    </a>
                                </li>
                            @endif

                            @if(Arr::exists($pages,$privacy_policy_page_id) && $slug = Arr::get($pages,$privacy_policy_page_id))
                                <li class="list-item">
                                    <a href="{{route("page.show",$slug)}}"
                                       class="list-link">
                                        @lang('Privacy policy')
                                    </a>
                                </li>
                            @endif
                            <li class="list-item">
                                <a href="{{ route('contact-us') }}" class="list-link">
                                    @lang('Contact us')
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                @if(Ecommerce::settings()->get('apple_store_app_url') || Ecommerce::settings()->get('google_store_app_url'))
                    <div class="col-lg-4">
                        <div class="download-content">
                            <h3 class="footer-head download-head">
                                @lang('Download an app') {{settings('app_name')}}
                            </h3>
                            <div class="download-links">
                                @if(Ecommerce::settings()->get('apple_store_app_url') )
                                    <a target="_blank" href="{{Ecommerce::settings()->get('apple_store_app_url')}}"
                                       class="download-link">
                                        <img src="{{Ecommerce::theme()->asset('shop/images/links/01.png')}}"
                                             class="img-fluid">
                                    </a>
                                @endif
                                @if(Ecommerce::settings()->get('google_store_app_url') )

                                    <a target="_blank" href="{{Ecommerce::settings()->get('google_store_app_url')}}"
                                       class="download-link">
                                        <img src="{{Ecommerce::theme()->asset('shop/images/links/02.png')}}"
                                             class="img-fluid">
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            @if(!is_null(settings('contact-maroof-link')))
                <div class="maroof">
                    <a href="{{ settings('contact-maroof-link') }}" class="maroof">
                        <img src="{{upload_storage_url(settings('contact-maroof-logo'))}}" class="img-fluid">
                    </a>
                </div>
            @endif
            @if($payments_img = Ecommerce::settings()->get("footer_image_payment"))
            <div class="payments">
                <img src="{{ upload_storage_url($payments_img) }}" alt=""
                     title=""
                     class="img-fluid">
            </div>
            @endif
            <p class="copyrights">
                © {{settings('ecommerce-site-name')}} {{ date('Y') }}, @lang('All rights reserved')
            </p>
        </div>
    </div>
</footer>
@if (!Ecommerce::auth()->check())
    <div class="modal fade login-modal" id="login-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('Login')</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="fas fa-times-circle"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="login-form">
                        @if (in_array(Ecommerce::settings()->get("ecommerce-login-by"),["phone-and-password","phone-only"]))
                            @include('theme::auth.login.by-phone')
                        @else
                            @include('theme::auth.login.by-email')
                        @endif
                        <a href="#!" class="submit-btn login_submit">
                            @lang('Login')
                        </a>
                    </form>
                </div>
                <div class="modal-footer">
                    <p class="register-text">
                        @lang("Forget password.") <a href="{{ route('forget-password.index') }}">@lang('Click here')</a>
                    </p>
                    <p class="register-text">
                        @lang("You don't have an account.") <a href="{{ route('register') }}">@lang('Register now')</a>
                    </p>
                    <a role="button" data-dismiss="modal" class="continue-text">
                        @lang('Or continue without registration')
                    </a>
                </div>
            </div>
        </div>
    </div>
@endif
@foreach([
Ecommerce::theme()->asset('lib/js/jquery.min.js'),
Ecommerce::theme()->asset('lib/js/popper.min.js'),
Ecommerce::theme()->asset('lib/js/bootstrap.min.js'),
Ecommerce::theme()->asset('lib/js/swiper.min.js'),
Ecommerce::theme()->asset('lib/js/flatpickr.min.js'),
Ecommerce::theme()->asset('shop/js/ar.js'),
Ecommerce::theme()->asset('lib/js/select2.min.js'),
'//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js',
Ecommerce::theme()->asset('shop/js/main.js'),
] as $script)
    <script src="{{$script}}"></script>
@endforeach
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': _token
        }
    });
    toastr.options = {
        closeButton: true,
        progressBar: true,
        positionClass: "toast-bottom-right",
        preventDuplicates: true,
        rtl: $('html').attr('dir') === 'rtl'
    }

    function notify(title, text, type = 'success') {
        toastr[type](text, title);
    }
    @if(session('should-login'))
    $('#login-modal').modal('show');
    @endif
    @if(session('cart'))
    notify("@lang('Error')", "{{ session('cart') }}", 'error');
    @endif
    @if(session('success'))
    notify("@lang('Success')", "{{ session('success') }}");
    @endif
    @if(session('error'))
    notify("@lang('Error')", "{{ session('error') }}", 'error');
    @endif
    @if (!Ecommerce::auth()->check())
    $('.login_submit').on('click', function () {
        $('.display_error').html("");
        $.post(route("login.check"), {
            object: $('.login_number').val(),
            password: $('.login_password').val(),
            _token: $('meta[name="csrf-token"]').attr('content')
        }, function (data) {
            if (data.status == 0) {
                notify("@lang('Error')", data.message, 'error');
            } else {
                notify("@lang('Done')", data.message);
                window.location.href = data.redirect;
            }
        })
            .fail(function (data) {
                if (data.status === 422) {
                    var errors = $.parseJSON(data.responseText).errors;
                    $.each(errors, function (key, value) {
                        notify("@lang('Error')", value[0], 'error');
                    });
                }
            });
    });
    @endif
    function updateCartCount(count) {
        $(".cart-items-count").html(count);
    }

    function changeIconState(selector, status = "loading") {
        let statusHtml = {
            loading: `<i class="fas fa-spinner"></i>`,
            success: `<i class="fas fa-check"></i>`,
            danger: `<i class="fas fa-times"></i>`,
            none: ""
        }
        $(selector).html(statusHtml[status]);
    }

    function getCartTotalSummation() {
        $.ajax({
            url: route("cart.get-total-summation"),
            success: function (data) {
                $("#cart-total-summation-container").html(data);
            }
        })
    }

    function replaceContentWith(selector, text) {
        $(selector).children().remove();
        $(selector).html(text);
    }

    function cartUpdated(cart) {
        @if(Route::currentRouteName() == "cart" )
        getCartTotalSummation()
        replaceContentWith("#cart-total-summation-container", $("#spinner").html());
        @endif
        updateCartCount(cart.quantity);
        notify("@lang('Done')", "@lang('Cart updated')");
    }

    function addToCart(item, quantity) {
        $.ajax({
            url: route("add-to-cart", [item, quantity]),
            type: "POST",
            success: function (cart) {
                cartUpdated(cart);
            },
            error: function (dd) {
                location.assign(dd.responseJSON['redirect']);
            }
        })
    }

    function updateCartItemQuantity(cartId, quantity) {
        $.ajax({
            url: route("update-cart-item-quantity", [cartId, quantity]),
            type: "POST",
            success: function (cart) {
                cartUpdated(cart);
            },
            error: function (dd) {
                location.assign(dd.responseJSON['redirect']);
            }
        })
    }

    $(function () {
        $('.qty-control').on("click", function () {
            let input = $(this).parents(".item-qty").find("input.qty-input");
            let quantity = input.val()
            let item = input.data("item");
            let cartId = input.data('cart-id');
            if (cartId) {
                updateCartItemQuantity(cartId, quantity)
            } else {
                addToCart(item, quantity);
            }
        });
        $(".item-qty input.qty-input").on("change", function () {
            let input = $(this);
            let quantity = input.val()
            let item = input.data("item");
            let cartId = input.data('cart-id');
            if (cartId) {
                updateCartItemQuantity(cartId, quantity)
            } else {
                addToCart(item, quantity);
            }
        });
        $("a.item-delete").on("click", function () {
            let that = $(this);
            let item = that.data("item");
            replaceContentWith("#cart-total-summation-container", $("#spinner").html());
            $.ajax({
                url: route("remove-item-from-cart", {item}),
                type: "DELETE",
                success: function (cart) {
                    if (cart.length === 0) {
                        replaceContentWith(".cart-content", $("#empty-cart").html());
                    }
                    getCartTotalSummation();
                    updateCartCount(cart.length);
                    that.parents(".cart-item").remove();
                    notify("@lang('Success')", "@lang('Item removed from cart')");

                }
            })
        })
        $(".discount-code-input").on("change", function () {
            let that = $(this);
            let coupon = that.val();
            changeIconState(that.parent().find(".code-btn-input"));
            $.ajax({
                url: route("cart.apply-coupon", {coupon}),
                type: "POST",
                success: function (data) {
                    if (data.status === 200) {
                        (coupon === "")
                            ? changeIconState(that.parent().find(".code-btn-input"), "none")
                            : changeIconState(that.parent().find(".code-btn-input"), "success");
                        replaceContentWith("#cart-total-summation-container", $("#spinner").html());
                        getCartTotalSummation();
                        notify("@lang('Success')", data.message);

                    } else {
                        changeIconState(that.parent().find(".code-btn-input"), "danger");
                        notify("@lang('Error')", data.message, "error");

                    }

                },
            })
        })
        $(".voucher-code-input").on("change", function () {
            let that = $(this);
            let coupon = that.val();
            changeIconState(that.parent().find(".code-btn-input"));
            $.ajax({
                url: route("cart.apply-voucher", {coupon}),
                type: "POST",
                success: function (data) {
                    if (data.status === 200) {
                        (coupon === "")
                            ? changeIconState(that.parent().find(".code-btn-input"), "none")
                            : changeIconState(that.parent().find(".code-btn-input"), "success");
                        replaceContentWith("#cart-total-summation-container", $("#spinner").html());
                        getCartTotalSummation();
                        notify("@lang('Success')", data.message);
                    } else {
                        changeIconState(that.parent().find(".code-btn-input"), "danger");
                        notify("@lang('Error')", data.message, "error");

                    }

                },
            })

        });
    });
</script>
@stack("modals")
@stack("scripts")
