<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<title>@yield('page_title')@if(Ecommerce::settings()->get("ecommerce-site-name")) - {{Ecommerce::settings()->get("ecommerce-site-name")}} @endif</title>
@if(Ecommerce::settings()->get('seo-head-description'))
    <meta name="description" content="{{Ecommerce::settings()->get('seo-head-description')}}">
@endif
<link rel='shortcut icon' type='img/png' href='{{Ecommerce::theme()->asset('shop/images/fav.png')}}'/>
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/bootstrap.min.css")}}"/>
@if (\App::getLocale() == 'ar')
    <link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/bootstrap-rtl.min.css")}}"/>
@endif
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/swiper.min.css")}}"/>
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/fontawesome.min.css")}}"/>
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/flatpickr.min.css")}}"/>
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/select2.min.css")}}"/>
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("shop/css/main.css")}}"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>
@stack("styles")
<style>
    .error-text-alert {color: red;display: inline-block;}
</style>
<script>
    var _token = '{{csrf_token()}}';
</script>
{!!Ecommerce::settings()->get('seo-head-scripts')!!}
