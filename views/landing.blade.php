<!DOCTYPE HTML>
<?php
$allowedLang = getAllowedLang();
$allowedLangHasMultiLang = count($allowedLang) > 1;
?>
<HTML dir="{{App::getLocale() == "ar" ? 'rtl' : "ltr"}}" lang="{{App::getLocale()}}">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no"/>
    <title>{{settings('company_name')}}</title>
    <link rel='shortcut icon' type='img/png' href='{{ Ecommerce::theme()->asset('landing/images/fav.png') }}'/>
    <link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/bootstrap.min.css")}}"/>
    <link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/swiper.min.css")}}"/>
    <link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/fontawesome.min.css")}}"/>
    <link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/animate.css")}}"/>
    @if(app()->getLocale() == "ar")
        <link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/bootstrap-rtl.min.css")}}"/>

    @endif
    <link rel="stylesheet" href="{{Ecommerce::theme()->asset("landing/main.css")}}"/>

    <style>
        .error-help-block {
            color: red;
        }
    </style>
    <script>
        let theme_assets_url = "{{Ecommerce::theme()->asset()}}";
    </script>
</head>

<body>
<!-- Start Header -->
<header>
    <div class="container-fluid">
        <div class="header">
            <a href="{{route("home")}}" class="logo">
                <img src="{{ Ecommerce::theme()->asset('landing/images/logo.png') }}" alt="yassir" class="img-fluid">
            </a>
            <nav>
                <ul class="mo-navbar list-unstyled">
                    <li class="nav-item">
                        <a role="button" data-href="#home" class="nav-link active">
                            @lang('Home')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="button" data-href="#about" class="nav-link">
                            @lang('About')
                        </a>
                    </li>
                    @if(isset($features) && $features->count())
                        <li class="nav-item">
                            <a role="button" data-href="#feats" class="nav-link">
                                @lang('Features')
                            </a>
                        </li>
                    @endif
                    @if(isset($services) && $services->count())
                        <li class="nav-item">
                            <a role="button" data-href="#services" class="nav-link">
                                @lang('Services')
                            </a>
                        </li>
                    @endif

                    @isset($branches)
                        <li class="nav-item">
                            <a role="button" data-href="#branches" class="nav-link">
                                @lang('Branches')
                            </a>
                        </li>
                    @endisset
                    <li class="nav-item">
                        <a role="button" data-href="#contact" class="nav-link">
                            @lang('Contact Us')
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="header-tools">
                <a href="mailto:{{ Ecommerce::settings()->get('ecommerce-site-email') }}" class="header-tool">
                    <i class="far fa-envelope"></i>
                    <span class="text">{{ Ecommerce::settings()->get('ecommerce-site-email') }}</span>
                </a>
                <a href="tel:{{ Ecommerce::settings()->get('contact-phone') }}" class="header-tool">
                    <i class="fas fa-phone"></i>
                    <span class="text">+{{ Ecommerce::settings()->get('contact-phone') }}</span>
                </a>
                @if($allowedLangHasMultiLang)
                    <div class="lang-cont">
                        <a role="button" class="lang-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="24px"
                                 height="24px">
                                <path fill-rule="evenodd"
                                      d="M21.551,5.254 C21.372,5.002 21.187,4.757 20.988,4.521 C18.841,1.943 15.609,0.300 12.001,0.300 C11.994,0.300 11.987,0.300 11.983,0.300 C11.978,0.300 11.975,0.300 11.973,0.300 C11.959,0.300 11.945,0.302 11.933,0.302 C8.349,0.321 5.147,1.961 3.012,4.521 C2.815,4.757 2.628,5.002 2.449,5.254 C1.097,7.162 0.300,9.487 0.300,12.000 C0.300,14.510 1.097,16.837 2.449,18.745 C2.628,18.996 2.815,19.242 3.012,19.478 C5.147,22.038 8.349,23.677 11.933,23.697 C11.945,23.697 11.959,23.700 11.973,23.700 C11.975,23.700 11.980,23.697 11.983,23.697 C11.987,23.697 11.994,23.700 12.001,23.700 C15.609,23.700 18.841,22.057 20.988,19.478 C21.185,19.242 21.372,18.996 21.551,18.745 C22.903,16.837 23.700,14.510 23.700,12.000 C23.700,9.487 22.903,7.162 21.551,5.254 L21.551,5.254 ZM8.579,1.821 C7.506,2.859 6.610,4.376 5.996,6.194 C5.168,5.887 4.428,5.514 3.786,5.095 C5.042,3.604 6.694,2.456 8.579,1.821 L8.579,1.821 ZM3.223,5.831 C3.952,6.316 4.793,6.740 5.722,7.089 C5.321,8.556 5.091,10.183 5.082,11.901 L1.261,11.901 C1.282,9.642 2.009,7.552 3.223,5.831 L3.223,5.831 ZM1.298,12.860 L5.100,12.860 C5.162,14.297 5.382,15.660 5.722,16.907 C4.791,17.259 3.950,17.683 3.223,18.169 C2.147,16.643 1.453,14.827 1.298,12.860 L1.298,12.860 ZM3.790,18.900 C4.430,18.482 5.171,18.112 5.993,17.802 C6.610,19.624 7.503,21.140 8.579,22.176 C6.694,21.541 5.044,20.393 3.790,18.900 L3.790,18.900 ZM11.521,22.703 C9.549,22.434 7.850,20.414 6.887,17.505 C8.277,17.088 9.849,16.835 11.521,16.795 L11.521,22.703 ZM11.521,15.836 C9.758,15.878 8.096,16.151 6.617,16.603 C6.314,15.454 6.120,14.191 6.062,12.860 L11.521,12.860 L11.521,15.836 ZM11.521,11.901 L6.043,11.901 C6.050,10.289 6.261,8.765 6.622,7.397 C8.098,7.849 9.761,8.121 11.521,8.163 L11.521,11.901 ZM11.521,7.204 C9.849,7.162 8.277,6.911 6.889,6.494 C7.852,3.586 9.552,1.565 11.521,1.293 L11.521,7.204 ZM22.737,11.901 L18.862,11.901 C18.855,10.190 18.625,8.571 18.227,7.108 C19.179,6.756 20.037,6.326 20.777,5.831 C21.991,7.553 22.718,9.642 22.737,11.901 L22.737,11.901 ZM20.212,5.098 C19.558,5.524 18.799,5.901 17.955,6.213 C17.334,4.369 16.427,2.833 15.334,1.793 C17.257,2.421 18.937,3.583 20.212,5.098 L20.212,5.098 ZM12.479,1.301 C14.430,1.602 16.108,3.621 17.060,6.508 C15.686,6.916 14.130,7.165 12.479,7.204 L12.479,1.301 ZM12.479,8.163 C14.221,8.123 15.862,7.856 17.327,7.413 C17.686,8.777 17.894,10.296 17.903,11.901 L12.479,11.901 L12.479,8.163 ZM12.479,12.860 L17.882,12.860 C17.824,14.184 17.632,15.440 17.332,16.586 C15.864,16.143 14.221,15.876 12.482,15.834 L12.482,12.860 L12.479,12.860 ZM12.479,22.697 L12.479,16.795 C14.132,16.835 15.686,17.081 17.064,17.491 C16.108,20.378 14.430,22.397 12.479,22.697 L12.479,22.697 ZM15.334,22.207 C16.427,21.166 17.336,19.633 17.957,17.786 C18.799,18.098 19.558,18.473 20.210,18.902 C18.935,20.414 17.257,21.576 15.334,22.207 L15.334,22.207 ZM20.777,18.169 C20.037,17.674 19.179,17.243 18.227,16.889 C18.565,15.647 18.782,14.290 18.843,12.860 L22.701,12.860 C22.547,14.827 21.855,16.643 20.777,18.169 L20.777,18.169 Z"/>
                            </svg>
                            <span class="lang-name">
                {{ __(Ecommerce::localization()->current()) }}
              </span>
                        </a>
                        <ul class="list-unstyled lang-menu">
                            @foreach ($allowedLang as $localeCode=>$properties)
                                @continue(Ecommerce::localization()->current() == $localeCode)
                                <li class="lang-item">
                                    <a rel="alternate"
                                       class="lang-link"
                                       data-toggle="tooltip"
                                       data-placement="top"
                                       title="{{ $properties['native'] }}"
                                       hreflang="{{ $localeCode }}"
                                       href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                        {{ $properties['name'] }}
                                    </a>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                @endif
            </div>
            <a role="button" class="menu-btn">
                <i class="fas fa-bars"></i>
            </a>
        </div>
    </div>
</header>
<!-- End Header -->

<!-- Start Main Slider -->
@if(isset($sliders) && count($sliders))
    <main class="main-slider-section" id="home">
        <div class="container-fluid">
            <div class="main-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">

                        @foreach($sliders as $slider)
                            <div class="swiper-slide">
                                <div class="slider-slide ">
                                    <div class="loading-img slider-img">
                                        <img class="lazy-img"
                                             data-src="{{upload_storage_url($slider->image)}}">
                                    </div>
                                    <div class="slider-text">
                                        <h6 class="slider-subtitle">
                                            {{$slider->title}}
                                        </h6>
                                        <h1 class="slider-maintitle">
                                            {{$slider->description}}
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>

                <div class="swiper-pagination"></div>
            </div>
        </div>
    </main>
@endif
<!-- End Main Slider -->

<!-- Start About Us -->
<section class="about-section" id="about">
    <div class="container">
        <div class="about-container">
            <h2 class="section-title wow fadeInUp">
                {{settings()->group("landing-page")->get("about-title-%lang%")}}
            </h2>
            <div class="loading-img about-img">
                <img class="lazy-img"
                     data-src="{{upload_storage_url(settings()->group("landing-page")->get("about-image"))}}">
            </div>
            <div class="about-content">
                <h2 class="section-title about-title wow fadeInUp">
                    {{settings()->group("landing-page")->get("about-title-%lang%")}}
                </h2>
                <p class="about-desc wow fadeInUp" data-wow-delay="0.2s">
                    {{settings()->group("landing-page")->get("about-description-%lang%")}}
                </p>
                <div class="about-states">
                    <div class="state-item">
                        <span class="item-icon fas fa-glasses"></span>
                        <strong class="item-number">
                            {{settings()->group("landing-page")->get("experience_years")}}
                        </strong>
                        <span class="item-name">@lang("Experience years")</span>
                    </div>
                    <div class="state-item">
                        <span class="item-icon fas fa-shopping-cart"></span>
                        <strong class="item-number">
                            {{settings()->group("landing-page")->get("available_products")}}
                        </strong>
                        <span class="item-name">@lang("Available products")</span>
                    </div>
                    <div class="state-item">
                        <span class="item-icon fas fa-users"></span>
                        <strong class="item-number">
                            {{settings()->group("landing-page")->get("happy_clients")}}
                        </strong>
                        <span class="item-name">@lang("Happy clients")</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Us -->

@if(isset($features) && count($features))
    <!-- Start Features -->
    <section class="feats-section" id="feats">
        <div class="container">
            <div class="feats-container">
                <h2 class="section-title wow fadeInUp">
                    {{settings()->group("landing-page")->get("features-title-%lang%")}}
                </h2>
                <h5 class="section-subtitle wow fadeInUp" data-wow-delay="0.2s">
                    {{settings()->group("landing-page")->get("features-description-%lang%")}}
                </h5>
                <div class="feats-slider">
                    <div class="swiper-container">
                        <div class="feats-content">
                            @foreach($features as $feature)
                                <div class="feat-item wow fadeInDown" data-wow-delay="0.1s">
                                    @if($feature->icon)
                                        <span class="item-icon {{$feature->icon}}"></span>

                                    @else
                                        <img class="my-1" width="50" height="50"
                                             src="{{upload_storage_url($feature->getFirstMedia())}}"

                                             alt="{{$feature->title}}">
                                    @endif
                                    <h5 class="item-name">
                                        {{$feature->title}}
                                    </h5>
                                    <p class="item-desc">
                                        {{$feature->description}}
                                    </p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Features -->
@endif

@if(isset($services) && count($services))
    <!-- Start Services -->
    <section class="services-section" id="services">
        <div class="container">
            <div class="services-container">
                <h2 class="section-title wow fadeInUp">
                    {{settings()->group("landing-page")->get("services-title-%lang%")}}
                </h2>
                <h5 class="section-subtitle wow fadeInUp" data-wow-delay="0.2s">
                    {{settings()->group("landing-page")->get("services-description-%lang%")}}
                </h5>
                <div class="services-slider">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($services as $service)
                                <div class="swiper-slide">
                                    <div class="service-cont">
                                        <div class="service-img">
                                            <img class="lazy-img"
                                                 data-src="{{upload_storage_url($service->getFirstMedia())}}">
                                        </div>
                                        <h3 class="service-title">
                                            {{$service->title}}
                                        </h3>
                                        <p class="service-desc">{{$service->description}} </p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Services -->
@endif

@if(isset($branches) && count($branches))
    <!-- Start Branches -->
    <section class="branches-section" id="branches">
        <div class="map" id="map" data-locations="{{$branches}}">
            <div id="map_cont" style="width: 100%; height: 100%;"></div>
        </div>
    </section>
    <!-- End Branches -->
@endif

<!-- Start Contact Us -->
@php($image = settings()->group("landing-page")->get("contact-image"))
<section class="contact-section" id="contact">
    <div class="container">
        <div class="contact-container"
             style="@if($image) grid-template-columns: 1fr 1fr; @else grid-template-columns: 1fr; @endif">
            <h2 class="section-title wow fadeInUp">
                {{settings()->group("landing-page")->get("contact-title-%lang%")}}
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                @endif
            </h2>
            <h5 class="section-subtitle wow fadeInUp" data-wow-delay="0.2s">
                {{settings()->group("landing-page")->get("contact-description-%lang%")}}
            </h5>
            <div class="contact-content">
                <h2 class="section-title contact-title wow fadeInUp">
                    {{settings()->group("landing-page")->get("contact-title-%lang%")}}
                </h2>
                <h5 class="section-subtitle contact-subtitle wow fadeInUp" data-wow-delay="0.2s">
                    {{settings()->group("landing-page")->get("contact-description-%lang%")}}
                </h5>
                <form class="contact-form" action="{{Ecommerce::contact()->data('post_url')}}" method="post"
                      style="@if(!$image) padding : 0; @endif">
                    @csrf
                    <div class="form-group wow fadeInUp">
                        <input type="text" name="name" class="form-control" placeholder="{{__('Name')}}"
                               value="{{old("name")}}">
                        @error('name') <span style="color:red">{{ $message }}</span> @enderror

                    </div>
                    <div class="form-group wow fadeInUp">
                        <input type="email" name="email" class="form-control" placeholder="{{__("Email")}}"
                               value="{{old("email")}}">
                        @error('email') <span style="color:red">{{ $message }}</span> @enderror

                    </div>
                    <div class="form-group wow fadeInUp" data-wow-delay="0.1s">
                        <input type="number" name="phone" class="form-control" placeholder="{{__('Phone')}}"
                               value="{{old('phone')}}">
                        @error('phone') <span style="color:red">{{ $message }}</span> @enderror

                    </div>

                    @if(isset($types)&&$types->count())
                        <div class="form-group wow fadeInUp">
                            <select class="form-control placeholder" data-style="btn-default btn-lg"
                                    style="height: 60px;"
                                    data-width="100%" name="contact_type_id">
                                <option value="" selected>@lang("Select Type")</option>
                                @foreach ($types as $type)
                                    <option
                                            @if(old("contact_type_id") == $type->id) selected @endif
                                    value="{{ $type->id }}" @if(old('contact_type_id') == $type->id) @endif>{{ $type->name }}</option>
                                @endforeach
                            </select>
                            @error('contact_type_id') <span style="color:red">{{ $message }}</span> @enderror
                        </div>
                    @endif
                    <div class="form-group wow fadeInUp" data-wow-delay="0.2s">
                        <input type="text" name="subject" class="form-control" placeholder="@lang('Subject')"
                               value="{{old('subject')}}">
                        @error('subject') <span style="color:red">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group wow fadeInUp" data-wow-delay="0.2s">
                        <textarea class="form-control" name="message"
                                  placeholder="{{__('Message')}}">{{old('message')}}</textarea>
                        @error('message') <span style="color:red">{{ $message }}</span> @enderror

                    </div>
                    <button class="btn btn-primary wow fadeInUp" data-wow-delay="0.3s">
                        @lang("Send")
                    </button>
                </form>
            </div>
            @if($image)
                <div class="contact-img about-img">
                    <img class="lazy-img"
                         data-src="{{upload_storage_url($image)}}">
                </div>
            @endif
        </div>
    </div>
</section>
<!-- End Contact Us -->

<!-- Start Footer -->
<?php
$links = [
    'instagram',
    'youtube',
    'twitter',
    'facebook'
];
?>
<footer>
    <div class="socials">
        @foreach(array_filter(json_decode(settings()->group("ecommerce")->get("social_links"))??[])??[] as $key=>$social )
            <a href="{{ $social->link }}" class="social">
                <i class="{{$social->icon}}"></i>
            </a>
        @endforeach
    </div>
    <div class="copyrights">
        &copy;{{config('app.name')}} {{ date('Y') }}, @lang('All rights reserved')
    </div>
</footer>

<div class="arrow-top">
    <span class="fas fa-arrow-up"></span>
</div>
<!-- End Footer -->

<script src="{{ Ecommerce::theme()->asset('lib/js/jquery.min.js') }}"></script>
<script src="{{ Ecommerce::theme()->asset('lib/js/bootstrap.min.js') }}"></script>
<script src="{{ Ecommerce::theme()->asset('lib/js/swiper.min.js') }}"></script>
<script src="{{ Ecommerce::theme()->asset('lib/js/wow.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9UeezZ2xyNjrwck8SLdh9NxsJp6HhLQc&sensor"></script>
<script src="{{ Ecommerce::theme()->asset('landing/map.js') }}"></script>
<script src="{{ Ecommerce::theme()->asset('landing/main.js') }}"></script>
<script src="{{ Ecommerce::theme()->asset('landing/plugins/jsvalidation/js/jsvalidation.js') }}"></script>

{!! JsValidator::formRequest('\Tasawk\Contacts\Requests\Front\ContactRequest',".contact-form") !!}
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@if(session()->has("success"))
    <script>
        alert("{{session()->get("success")}}")
    </script>
@endif
<script>
    $(".contact-form").on("submit", function (e) {
        e.preventDefault();
        let form = $(this);
        let formData = form.serialize();
        let url = form.attr("action");
        $.ajax({
            url,
            type: "post",
            data: formData,
            success: function (data) {
                Swal.fire({
                    title: "{{__('Success')}}",
                    text: "{{__('Successfully send')}}",
                    icon: 'success',
                    confirmButtonText: "{{__('Okey')}}",
                    confirmButtonColor: "#fb6e20"
                });
                form[0].reset();
            }
        })
    })

</script>
</body>
</HTML>
