<?php

?>
<div class="item">
    <div class="item-img-container">
        <a href="{{route("shop.show",[Str::plural($item->type),$item->id])}}"
           class="loading-img item-img">
            <img
                    alt="{{ $item->title }}"
                    title="{{ $item->title }}"
                    class="lazy-img"
                    data-src="{{ upload_storage_url($item->getFirstMedia('items_main_image'))}}">
        </a>
    </div>
    <h5 class="item-name-container">
        <a href="{{route("shop.show",[Str::plural($item->type),$item->id])}}"
           class="item-name">
            {{ $item->title }}
        </a>
    </h5>
    <div class="prices">
        @if($item->hasOffer())
            <del class="old-price">
                {{ Ecommerce::formatPrice($item->pricing->originalPrice()) }} {{Ecommerce::currentSymbol()}}
            </del>
            <strong class="price">
                {{ Ecommerce::formatPrice($item->pricing->finalPrice()) }} {{Ecommerce::currentSymbol()}}
            </strong>
        @else
            <strong class="price">
                {{ Ecommerce::formatPrice($item->pricing->finalPrice()) }} {{Ecommerce::currentSymbol()}}
            </strong>
        @endif
    </div>
    <div class="item-qty">
        <a role="button" class="qty-control qty-plus"><i class="fas fa-plus-circle"></i></a>
        <a role="button" class="qty-control qty-minus"><i class="fas fa-minus-circle"></i></a>
        <input type="number"
               class="qty-input"
               value="{{Cart::getQuantityByModelId($item->id)}}"
               data-item="{{$item->id}}"
               @if($item->type !== "service" && $item->quantity > 0)
               data-max="{{ $item->quantity}}"
               @endif
               data-min="0">
    </div>
    @if($item->hasOffer())
        <span class="sale">%{{$item->pricing->offerRatio()}}</span>
    @endif
</div>
