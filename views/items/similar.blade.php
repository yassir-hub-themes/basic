@if($similar_items->count())
    <div class="related-content">
        <div class="section-head">
            <h3 class="section-title">
                @lang("Similar ".$type)
            </h3>
        </div>
        <div class="services-slider">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    @foreach($similar_items as $item)
                        <div class="swiper-slide">
                            <div class="item">
                                <div class="item-img-container">
                                    <a href="{{route("shop.show",[Str::plural($item->type),$item->id])}}"
                                       class="loading-img item-img">
                                        <img class="lazy-img"
                                             data-src="{{ upload_storage_url($item->getImage())}}">
                                    </a>
                                </div>
                                <h5 class="item-name-container">
                                    <a href="{{route("shop.show",[Str::plural($item->type),$item->id])}}"
                                       class="item-name">
                                        {{$item->title}}
                                    </a>
                                </h5>
                                <div class="prices">
                                    @if($item->hasOffer())
                                        <del class="old-price">
                                            {{ Ecommerce::formatPrice($item->pricing->originalPrice()) }} {{Ecommerce::currentSymbol()}}
                                        </del>
                                        <strong class="price">
                                            {{ Ecommerce::formatPrice($item->pricing->finalPrice())}} {{Ecommerce::currentSymbol()}}
                                        </strong>
                                    @else
                                        <strong class="price">
                                            {{ Ecommerce::formatPrice($item->pricing->finalPrice())}}{{Ecommerce::currentSymbol()}}
                                        </strong>
                                    @endif
                                </div>
                                <div class="item-qty">
                                    <a role="button" class="qty-control qty-plus">
                                        <i class="fas fa-plus-circle"></i>
                                    </a>
                                    <a role="button" class="qty-control qty-minus">
                                        <i class="fas fa-minus-circle"></i>
                                    </a>
                                    <input type="number" class="qty-input"
                                           value="{{(!is_null(Cart::get($item->id)))?Cart::get($item->id)->quantity:0}}"
                                           data-max="{{$item->quantity}}"
                                           data-item="{{$item->id}}"
                                           data-min="0"
                                           data-has-application="{{!is_null($item->customer_application_id) && $item->customer_application_id != 0}}"
                                           data-in-cart="{{Cart::has($item->id)}}"
                                           data-redirect-url="{{route("shop.show",[Str::plural($item->type),$item->id])}}"
                                    >
                                </div>
                                @if($item->hasOffer())
                                    <span class="sale">
                                                   %{{$item->pricing->offerRatio()}}
                                                </span>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="swiper-btn-next swiper-btn"><span class='fas fa-chevron-left'></span></div>
            <div class="swiper-btn-prev swiper-btn"><span class='fas fa-chevron-right'></span></div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
@endif
