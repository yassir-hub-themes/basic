@if($item->customerApplication->data??[])
    <h3 class="section-title">@lang("Application questions")</h3>
@endif
@if(!Cart::has($item->id))
    @foreach($item->customerApplication->data??[] as $question)
        <div class="form-group single-option @if(isset($question['required']) && $question['required'] =="true")required @endif">
            <label class="option-label">{{Arr::get($question,'lang_'.app()->getLocale(),$question['label'])}}</label>
            @includeWhen(in_array($question['type'],["file","image","barcode"]),"theme::partials.application_form_inputs._file")
            @includeWhen($question['type'] == "textarea","theme::partials.application_form_inputs._textarea")
            @includeWhen($question['type'] =="checkbox-group","theme::partials.application_form_inputs._checkbox")
            @includeWhen($question['type'] =="select","theme::partials.application_form_inputs._select")
            @includeWhen($question['type'] =="radio-group","theme::partials.application_form_inputs._radio")
            @includeWhen(in_array($question['type'],['text','number','date']),"theme::partials.application_form_inputs._other")
        </div>
    @endforeach
@endif
