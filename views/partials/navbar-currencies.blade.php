<?php
$currencies = \Tasawk\Currencies\Models\Currency::where('active', 1)->get();
if (count($currencies) <= 1) {
    return;
}
?>
<div class="user-cont pr-4">
    <a role="button" class="user-icon header-icon">
        <i class="fas  fa-dollar-sign"></i>
    </a>
    <div class="drop-down">
        @foreach( $currencies as $currency)
            @php($is_active =session()->get("current_currency") == $currency->code )
            <a
                    @if($is_active)
                    style="color: var(--main-color); text-decoration: none; "
                    onclick="return false;"
                    @endif
                    class=" @if($is_active) active @endif" href="{{ route('change-currency',$currency->code) }}">
                {{$currency->code}}
            </a>
        @endforeach
    </div>
</div>
