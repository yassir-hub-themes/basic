<h4 class="qty-cart-title">
    @lang("Choose quantity")
</h4>
<div class="item-qty single-item-qty">
    <a role="button" class="single-qty-control qty-plus">
        <i class="fas fa-plus-circle"></i>
    </a>
    <a role="button" class="single-qty-control qty-minus">
        <i class="fas fa-minus-circle"></i>
    </a>
    <input type="number" class="single-qty-input qty-input"
           name="quantity"
           value="{{Cart::getQuantityByModelId($item->id)}}"
           @if($item->type !== "service" && $item->quantity > 0)
           data-max="{{$item->quantity}}"
           @endif
           data-item="{{$item->id}}"
           data-min="1"
           required
    >
</div>
<button class="item-addToCart" data-item-id="{{$item->id}}">
    @lang("Add to cart")
</button>