

@foreach($question['values'] as $list)
    <div class="form-group">

        <input id="{{$question['name']}}-{{$list['value']}}"  type="checkbox" name="application[{{$question['name']}}][]" value="{{$list['value']}}">

        <label for="{{$question['name']}}-{{$list['value']}}">{{$list['label']}}</label>
    </div>
@endforeach

