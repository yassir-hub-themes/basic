<div class="single-input-file-container">
    <input type="file" onChange="uploadImg(this);"
           name="application[{{$question['name']}}]"
           @if($question['required'] == "true")
           required
           @endif
           @if(isset($question['multiple']) && $question['multiple'] =="true") multiple @endif
           @if($question['type'] =="image")
           accept="image/*"
           @else
           accept=".doc,.pdf"
            @endif
    >
    <span class="single-filename"></span>
    <a role="button" class="single-file-btn">
        <i class="fas fa-upload"></i>
        <span class="text">@lang("Upload")</span>
    </a>
</div>