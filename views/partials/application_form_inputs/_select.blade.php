{{--@dd($question)--}}
<div class="form-group">
    <select name="application[{{$question['name']}}]@if($question['multiple'] =="true")[]@endif" class="form-control" @if($question['multiple'] =="true") multiple @endif @if($question['required'] =="true") required @endif>
        @foreach($question['values'] as $list)
            <option value="{{$list['value']}}">{{$list['label']}}</option>
        @endforeach
    </select>
</div>

