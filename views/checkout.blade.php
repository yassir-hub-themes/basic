@extends("theme::layouts.master")
@section('page_title',__('Checkout'))
@push('styles')
@endpush
@section("content")
    <section class="content-section checkout-content">
        <div class="container">
            <h2 class="page-title">
                @lang('Checkout')
            </h2>
            <form action="{{ route('checkout.store') }}" method="POST" id="checkout-store" autocomplete="off">
                @csrf
                @if (!Ecommerce::auth()->check())
                    @include('theme::checkout.register')
                @endif
                @include('theme::checkout.map')
                <p class="error-text-alert map-error-msg"></p>
                <br>
                <br>
                @error('address.address_name')
                <span class="error-text-alert">{{$message}}</span>
                @enderror
                @include('theme::checkout.payment-methods')
                @include('theme::checkout.delivery-time',['opening_days'=>$opening_days])
                <a class="cart-btn btn_send">
                    @lang('Checkout')
                </a>
            </form>
        </div>
    </section>
    @include('theme::checkout.modals.address-book')
@endsection
@push("scripts")
    <script>
        $(function () {
            $(".cart-btn").on("click", function (e) {
                e.preventDefault()
                // if (!errors) {
                $('#checkout-store').submit();
                // }
            });
        });
    </script>
@endpush
