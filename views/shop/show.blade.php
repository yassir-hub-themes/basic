@php
    use Tasawk\Items\Model\Items;
@endphp
@extends("theme::layouts.master")
@section('page_title',$item->title)
@push('styles')
    <link rel="stylesheet" href="{{Ecommerce::theme()->asset("lib/css/fancybox.css")}}">
@endpush
@push('scripts')
    <script src="{{Ecommerce::theme()->asset("lib/js/fancybox.js")}}"></script>
@endpush
@section("content")
    <section class="content-section single-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="product-imgs-cont">
                        <div class="swiper-container product-imgs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="single-img-container">
                                        <a data-fancybox="post" href="{{ upload_storage_url($item->getImage())}}"
                                           class="loading-img single-img">
                                            <img class="lazy-img" data-src="{{ upload_storage_url($item->getImage())}}">
                                        </a>
                                    </div>
                                </div>
                                @foreach($item->getOtherImages() as $image)
                                    <div class="swiper-slide">
                                        <div class="single-img-container">
                                            <a data-fancybox="post" href="{{upload_storage_url($image)}}"
                                               class="loading-img single-img">
                                                <img class="lazy-img" data-src="{{upload_storage_url($image)}}">
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                        <div class="swiper-container product-thumbs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="thumb-cont">
                                        <div class="loading-img thumb-img">
                                            <img class="lazy-img" data-src="{{ upload_storage_url($item->getImage())}}">
                                        </div>
                                    </div>
                                </div>
                                @foreach($item->getOtherImages() as $image)
                                    <div class="swiper-slide">
                                        <div class="thumb-cont">
                                            <div class="loading-img thumb-img">
                                                <img class="lazy-img" data-src="{{upload_storage_url($image)}}">
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-btn-next swiper-btn"><span class='fas fa-chevron-left'></span></div>
                            <div class="swiper-btn-prev swiper-btn"><span class='fas fa-chevron-right'></span></div>
                        </div>
                        @if($item->hasOffer())
                            <span class="single-sale">
                                %{{$item->pricing->offerRatio()}}
                            </span>
                        @endif
                    </div>
                </div>


                <div class="col-lg-6">
                    <div class="single-info-container">
                        <h2 class="single-name">
                            {{$item->title}}
                        </h2>
                        <p class="single-desc">
                            {!!$item->description!!}
                        </p>
                        <div class="single-prices">
                            @if($item->hasOffer())
                                <del class="single-old-price">
                                    {{ Ecommerce::formatPrice($item->pricing->originalPrice()) }} {{Ecommerce::currentSymbol()}}
                                </del>
                                <strong class="single-price">
                                    {{ Ecommerce::formatPrice($item->pricing->finalPrice())}} {{Ecommerce::currentSymbol()}}
                                </strong>
                            @else
                                <strong class="single-price">
                                    {{ Ecommerce::formatPrice($item->pricing->finalPrice()) }} {{Ecommerce::currentSymbol()}}
                                </strong>
                            @endif
                        </div>
                        <span class="tax-hint">
                    @lang("The price includes tax")
                    </span>
                        <div class="single-options mb-4">
                            @include("theme::partials.item-form")
                        </div>

                    </div>
                </div>
            </div>
            @if($similar_items->count())
                <div class="related-content">
                    <div class="section-head">
                        <h3 class="section-title">
                            @lang("Similar ".$type)
                        </h3>
                    </div>
                    <div class="services-slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                @foreach($similar_items as $item)
                                    <div class="swiper-slide">
                                        <div class="item">
                                            <div class="item-img-container">
                                                <a href="{{route("shop.show",[Str::plural($item->type),$item->id])}}"
                                                   class="loading-img item-img">
                                                    <img class="lazy-img"
                                                         data-src="{{ upload_storage_url($item->getImage())}}">
                                                </a>
                                            </div>
                                            <h5 class="item-name-container">
                                                <a href="{{route("shop.show",[Str::plural($item->type),$item->id])}}"
                                                   class="item-name">
                                                    {{$item->title}}
                                                </a>
                                            </h5>
                                            <div class="prices">
                                                @if($item->hasOffer())
                                                    <del class="old-price">
                                                        {{ Ecommerce::formatPrice($item->pricing->originalPrice()) }} {{Ecommerce::currentSymbol()}}
                                                    </del>
                                                    <strong class="price">
                                                        {{ Ecommerce::formatPrice($item->pricing->finalPrice())}} {{Ecommerce::currentSymbol()}}
                                                    </strong>
                                                @else
                                                    <strong class="price">
                                                        {{ Ecommerce::formatPrice($item->pricing->finalPrice())}}{{Ecommerce::currentSymbol()}}
                                                    </strong>
                                                @endif
                                            </div>
                                            <div class="item-qty">
                                                <a role="button" class="qty-control qty-plus">
                                                    <i class="fas fa-plus-circle"></i>
                                                </a>
                                                <a role="button" class="qty-control qty-minus">
                                                    <i class="fas fa-minus-circle"></i>
                                                </a>
                                                <input type="number" class="qty-input"
                                                       value="{{Cart::getQuantityByModelId($item->id)}}"
                                                       data-max="{{$item->quantity}}"
                                                       data-item="{{$item->id}}"
                                                       data-min="1"
                                                       data-has-application="{{!is_null($item->customer_application_id) && $item->customer_application_id != 0}}"
                                                       data-in-cart="{{Cart::has($item->id)}}"
                                                       data-redirect-url="{{route("shop.show",[Str::plural($item->type),$item->id])}}"
                                                >
                                            </div>
                                            @if($item->hasOffer())
                                                <span class="sale">
                                                   %{{$item->pricing->offerRatio()}}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="swiper-btn-next swiper-btn"><span class='fas fa-chevron-left'></span></div>
                        <div class="swiper-btn-prev swiper-btn"><span class='fas fa-chevron-right'></span></div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection
