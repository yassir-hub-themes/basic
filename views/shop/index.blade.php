@extends("theme::layouts.master")
@section('page_title',__('Shop'))
@section("content")
    <!-- Start Page Content -->
    <section class="content-section list-content">
        <div class="container">
            <h2 class="page-title">
                @lang(ucfirst($type))
            </h2>
            @if($lists && count($lists))
                <div class="items-grid">
                    @foreach($lists as $item)
                        <div class="item-grid">
                            @include('theme::items.item')
                        </div>
                    @endforeach
                </div>
                {{$lists->links('theme::partials.pagination')}}
            @else
                <p>@lang("No data found")</p>
            @endif

        </div>
    </section>
    <!-- End Page Content -->
@endsection
