<?php

use Tasawk\Ecommerce\Lib\Options\Option;

?>
@extends("theme::layouts.master")
@section('page_title',__('Cart'))
@section('body_class','cart')
@section("content")
    @if($cart_items->count())
        <section class="content-section cart-content">
            <div class="container">
                <h2 class="page-title">
                    @lang("Cart")
                </h2>
                <div class="cart-items">
                    @foreach($cart_items as $item)
                        <div class="cart-item">
                            <a href="{{route("shop.show",[Str::plural($item->associatedModel->type),$item->associatedModel->id])}}"
                               class="loading-img cart-img">
                                <img class="lazy-img"
                                     data-src="{{ upload_storage_url($item->associatedModel->getFirstMedia('items_main_image'))}}">
                            </a>
                            <div class="cart-item-info">
                                <a href="{{route("shop.show",[Str::plural($item->associatedModel->type),$item->associatedModel->id])}}"
                                   class="cart-name font-weight-bold">
                                    {{$item->name}}
                                </a>
                                <ul class="list-unstyled">
                                    @foreach($item->attributes['options'] as $attribute)
                                        <li class="item-option">
                                            <span class="item-option-name">{{$attribute['name']}}</span>
                                            <span class="item-option-separator"> : </span>
                                            @if($attribute['type'] == 'image')
                                                <a target="_blank"
                                                   href="{{ upload_storage_url($attribute['value'])}}">@lang('File')</a>
                                            @else
                                                <span class="item-option-value">{{$attribute['value']}}</span>
                                            @endif
                                            @if($attribute['price'] != 0)
                                                <span class="item-option-separator"> : </span>
                                                <span class="item-option-price">{{Ecommerce::formatPrice($attribute['price'])}}</span>
                                                <span class="item-option-price-symbol">{{Ecommerce::currentSymbol()}}</span>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="qty-price">
                                    <div class="item-qty">
                                        <a role="button" class="qty-control qty-plus">
                                            <i class="fas fa-plus-circle"></i>
                                        </a>
                                        <a role="button" class="qty-control qty-minus">
                                            <i class="fas fa-minus-circle"></i>
                                        </a>
                                        <input type="number" class="qty-input" value="{{$item->quantity}}"
                                               @if($item->associatedModel->type !== "service" && $item->associatedModel->quantity > 0)
                                               data-max="{{$item->associatedModel->quantity}}"
                                               @endif
                                               data-min="1"
                                               data-item="{{$item->associatedModel->id}}"
                                               data-cart-id="{{$item->id}}">
                                    </div>
                                    <strong class="item-price">
                                        {{Ecommerce::formatPrice($item->getPriceWithConditions())}} {{Ecommerce::currentSymbol()}}
                                    </strong>
                                    <a role="button" class="item-delete" data-item="{{$item->id}}">
                                        <i class="fas fa-times-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <hr>
            <div class="container">
                <div class="code-inputs">
                    <div class="code-input-cont">
                        <label>

                            @lang("Discount coupon")

                        </label>
                        <div class="code-input">
                            <input class="active discount-code-input" type="text"
                                   placeholder="@lang('Type the discount coupon code here')"
                                   value="{{optional(Cart::getConditionsByType("coupon")->first())->getName()}}">
                            <button class="code-btn-input "></button>
                        </div>
                    </div>
                    <div class="code-input-cont">
                        <label>
                            @lang("Purchase coupons")
                        </label>
                        <div class="code-input">
                            <input type="text" class=" active voucher-code-input"
                                   placeholder="@lang('Type your voucher code here')"
                                   value="{{optional(Cart::getConditionsByType("voucher")->first())->getName()}}">
                            <button class="code-btn-input">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div id="cart-total-summation-container">
                @include("theme::partials.cart-total-summation")
            </div>
            <a href="{{ route('checkout') }}" class="cart-btn">
                @lang('Checkout')
            </a>
        </section>
    @else
        <section class="content-section">
            <div class="container">
                <div class="status-flex lazy-head-img">
                    <div class="status-img loaded-img">
                        <i class="fas fa-shopping-basket" style=" font-size: 7rem; color: gray;"></i>
                    </div>
                    <h1 class="status">
                        @lang("Your cart is empty")
                    </h1>
                    <a href="{{route('home')}}" class="cart-btn">
                        @lang('Go to home')
                    </a>
                </div>
            </div>
        </section>
    @endif
@endsection
@push("styles")
    <style>
        .spinner-wrapper {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 250px;
            width: 100%;
        }
    </style>
@endpush
@push("scripts")
    <template id="spinner">
        <div class="container">
            <div class="spinner-wrapper">
                <div class="spinner-border" role="status">
                    <span class="sr-only">@lang("Loading")...</span>
                </div>
            </div>
        </div>
    </template>
    <template id="empty-cart">
        <section class="content-section">
            <div class="container">
                <div class="status-flex lazy-head-img">
                    <div class="status-img loaded-img">
                        <i class="fas fa-shopping-basket" style=" font-size: 7rem; color: gray;"></i>
                    </div>
                    <h1 class="status">
                        @lang("Your cart is empty")
                    </h1>
                    <a href="{{route('home')}}" class="cart-btn">
                        @lang('Go to home')
                    </a>
                </div>
            </div>
        </section>
    </template>
@endpush
