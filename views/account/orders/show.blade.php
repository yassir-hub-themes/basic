@extends("theme::layouts.master")
@section('page_title',__('Show order'))
@section("content")

    <!-- Start Page Content -->
    <section class="content-section order-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('theme::account.menu')
                </div>
                <div class="col-lg-9">
                    <div class="acc-head">
                        <h2 class="acc-title">
                            @lang('Order details')
                        </h2>
                        <a href="{{ route('orders.reorder',$order->id) }}" class="reorder">
                            <i class="fas fa-redo"></i>
                            @lang('Reorder')
                        </a>
                    </div>
                    <div class="single-order">
                        <div class="order-head">
                            <div class="order-no">
                                @lang('Order number'). <span>{{ $order->id }}</span>
                            </div>
                            <div class="order-date">
                                {{ $order->created_at->format('M d, Y') }}
                            </div>
                        </div>
                        <div class="order-progress">
                            <div class="order-step @if(in_array($order->status,['new','in_processing','completed'])) done @endif">
                                <div class="step-icon">
                                    <i class="fas fa-check"></i>
                                </div>
                                <span class="step-name">
                                    @lang('In progress')
                                </span>
                            </div>
                            <div class="order-step @if(in_array($order->status,['in_processing','completed'])) done @endif">
                                <div class="step-icon">
                                    <i class="fas fa-check"></i>
                                </div>
                                <span class="step-name">
                                    @lang('Out for delivery')
                                </span>
                            </div>
                            <div class="order-step @if($order->status == 'completed') done @endif">
                                <div class="step-icon">
                                    <i class="fas fa-check"></i>
                                </div>
                                <span class="step-name">
                                    @lang('Delivered')
                                </span>
                            </div>
                        </div>
                        <div class="order-summary">
                            <?php
                            $sub_total = 0;
                            ?>
                            @foreach ($order->items()->whereIn('type', ['product', 'service'])->get() as $item)
                                <div class="summry-item">
                                    <div class="order-name">
                                        <a href="{{route("shop.show",[Str::plural($item->type),optional($item->products)->id])}}"
                                           class="name">
                                            {{ $item->products->title ?? '--'}}
                                        </a>
                                        <div class="single-qty-price">
                                            <div class="order-qty">
                                                x {{ $item->qty }}
                                            </div>
                                            <div class="single-price">
                                                {{ $item->cost }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="order-price">
                                        {{ $item->cost * $item->qty }}
                                    </div>
                                </div>
                                <?php
                                $sub_total = $sub_total + $item->cost * $item->qty;
                                ?>
                            @endforeach
                        </div>
                        <?php
                        $vat = $order->items->where('type', 'vat')->sum('cost');
                        $delivery = $order->items->where('type', 'delivery')->sum('cost');
                        $voucher = $order->items->where('type', 'voucher')->sum('cost');
                        $Coupon = $order->items->where('type', 'coupon')->sum('cost');
                        ?>
                        <div class="totals">
                            <div class="total">
                                <span class="title">
                                    @lang('Total demand')
                                </span>
                                <span class="value">
                                    {{ $sub_total }}
                                </span>
                            </div>
                            <div class="total">
                                <span class="title">
                                    @lang('Delivery cost')
                                </span>
                                <span class="value">
                                    {{ $delivery }}
                                </span>
                            </div>
                            <div class="total">
                                <span class="title">
                                    @lang('Value added tax')
                                </span>
                                <span class="value">
                                    {{ $vat }}
                                </span>
                            </div>
                            <div class="total">
                                <span class="title">
                                    @lang('Total')
                                </span>
                                <span class="value">
                                    {{ $order->total }}
                                </span>
                            </div>
                        </div>
                        <div class="payment">
                            <h4 class="title">
                                @lang('Payment method') ( {{ __($order->payment_method) }} )
                            </h4>
                            <div class="method">
                                <?php
                                $payments = json_decode(settings()->group("ecommerce")->get("allowed_payment_methods"), true) ?? [];
                                ?>
                                <img src="{{ upload_storage_url(Ecommerce::settings()->get("$order->payment_method-image")) }}"
                                     class="img-fluid">
                            </div>
                        </div>
                        <div class="location">
                            <h4 class="title">
                                @lang('Delivery address')
                            </h4>
                            <div class="order-map">
                                <div id="order-map"></div>
                                <div class="locationInput-container">
                                    <input disabled type="text" class="locationInput"
                                           value="<?php echo e($order->map_address['address_name']) ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Page Content -->


@endsection
@push('scripts')
    <script
            src="https://maps.googleapis.com/maps/api/js?region=SA&language={{ \App::getLocale() }}&key={{ settings()->group('third-party')->get('google_map_key') }}&libraries=places">
    </script>

    <script>
        $(document).ready(function () {
            initMap()
        });

        function initMap() {
            const myLatLng = {
                lat: parseFloat("<?php echo e($order->map_address['lat']) ?>"),
                lng: parseFloat("<?php echo e($order->map_address['lang']) ?>")
            };
            let map = new google.maps.Map(document.getElementById("order-map"), {
                center: myLatLng,
                zoom: 17,
            });
            let marker = new google.maps.Marker({
                position: map.getCenter(),
                map,
                icon: "{{Ecommerce::theme()->asset('shop/images/pin.png')}}"
            });
        }
    </script>
@endpush