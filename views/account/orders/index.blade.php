@extends("theme::layouts.master")
@section('page_title',__('Orders'))
@section("content")

    <!-- Start Page Content -->
    <section class="content-section my-acc-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('theme::account.menu')
                </div>
                <div class="col-lg-9">
                    <div class="acc-head">
                        <h2 class="acc-title">
                            @lang('Orders')
                        </h2>
                    </div>
                    <div class="orders-list">
                        @foreach (Ecommerce::auth()->user()->orders()->orderBy('id','DESC')->get() as $order)
                            <div class="order-item">
                                <div class="order-head">
                                    <div class="order-no">
                                        @lang('Order number') <span>{{ $order->id }}</span>
                                    </div>
                                    <div class="order-date">
                                        {{ $order->created_at->format('M d, Y') }}
                                    </div>
                                </div>
                                <div class="order-progress">
                                    <div class="order-step @if(in_array($order->status,['new','in_processing','completed'])) done @endif">
                                        <div class="step-icon">
                                            <i class="fas fa-check"></i>
                                        </div>
                                        <span class="step-name">
                                            @lang('In progress')
                                        </span>
                                    </div>
                                    <div class="order-step @if(in_array($order->status,['in_processing','completed'])) done @endif">
                                        <div class="step-icon">
                                            <i class="fas fa-check"></i>
                                        </div>
                                        <span class="step-name">
                                            @lang('Out for delivery')
                                        </span>
                                    </div>
                                    <div class="order-step @if($order->status == 'completed') done @endif">
                                        <div class="step-icon">
                                            <i class="fas fa-check"></i>
                                        </div>
                                        <span class="step-name">
                                            @lang('Delivered')
                                        </span>
                                    </div>
                                </div>
                                <div class="order-summary">
                                    @foreach ($order->items()->whereIn('type', ['product', 'service'])->get() as $item)

                                        <div class="summry-item">
                                            <div class="order-qty">
                                                x {{ $item->qty }}
                                            </div>
                                            <div class="order-name">
                                                <a href="{{route("shop.show",[Str::plural($item->type),optional($item->products)->id])}}"
                                                   class="name">
                                                    {{ $item->products->title ?? ''}}
                                                </a>
                                            </div>
                                            <div class="order-price">
                                                {{ $item->cost * $item->qty }}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="details-link-cont">
                                    <a href="{{ route('orders.show',$order->id) }}" class="order-details">
                                        @lang('Order details')
                                        <i class="fas fa-chevron-{{app()->getLocale() == 'ar'?'left':'right'}}"></i>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Page Content -->


@endsection
@push('scripts')

@endpush