@extends("theme::layouts.master")
@section('page_title',__('Edit address'))
@section("content")

    <!-- Start Page Content -->
    <section class="content-section address-book-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('theme::account.menu')
                </div>
                <div class="col-lg-9">
                    <div class="acc-head">
                        <h2 class="acc-title">
                            @lang('Edit address')
                        </h2>
                    </div>
                    <form class="address-book-form" method="POST" action="{{ route('addresses.edit',$addresse->id) }}">
                        @csrf
                        <div class="float-field">
                            @error('name')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="text" value="{{old('name',$addresse->name)}}" name="name"
                                   placeholder="@lang('Full name')">
                            <label>@lang('Full name')</label>
                        </div>
                        <div class="float-field">
                            @error('phone')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="number" value="{{old('phone',$addresse->phone)}}" name="phone"
                                   placeholder="@lang('Phone number')">
                            <label>@lang('Phone number')</label>
                        </div>

                        <div class="float-field">
                            @error('type')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <select class="new-address-select" name="type">
                                <option value="">@lang("Select type")</option>
                                <option @if(old("type",$addresse->type) == "primary")  selected
                                        @endif value="primary">@lang("Primary")</option>
                                <option @if(old("type",$addresse->type) == "secondary")  selected
                                        @endif value="secondary">@lang("Secondary")</option>
                                <option @if(old("type",$addresse->type)== "shipping")  selected
                                        @endif value="shipping">@lang("Shipping")</option>
                            </select>
                            <label>@lang('Type')</label>
                        </div>

                        <div class="float-field">
                            @error('country_id')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <select class="new-address-select country-select" name="country_id">
                                <option disabled>@lang("Select country")</option>
                                @foreach (\Tasawk\Locations\Models\Country::all() as $country)
                                    <option @if ($addresse->country_id == $country->id)
                                            selected
                                            @endif value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                            <label>@lang('Country')</label>
                        </div>

                        <div class="float-field">
                            @error('city_id')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <select class="new-address-select city-select" name="city_id">
                                <option disabled>@lang("Select city")</option>
                                @foreach ($addresse->country->cities as $city)
                                    <option @if ($addresse->city_id == $city->id)
                                            selected
                                            @endif value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            </select>
                            <label>@lang('City')</label>
                        </div>

                        <div class="float-field">
                            @error('state')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="text" name="state" value="{{ old('state',$addresse->state) }}"
                                   placeholder="@lang('State')">
                            <label>@lang('State')</label>
                        </div>

                        <div class="float-field">
                            @error('street')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="text" name="street" value="{{ old('street',$addresse->street) }}"
                                   placeholder="@lang('Street name')">
                            <label>@lang('Street name')</label>
                        </div>

                        <div class="float-field">
                            @error('zip_code')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="text" name="zip_code" value="{{ old('zip_code',$addresse->zip_code) }}"
                                   placeholder="@lang('Zip code')">
                            <label>@lang('Zip code')</label>
                        </div>

                        <div class="float-field">
                            @error('note')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="text" name="note" value="{{ old('note',$addresse->note) }}"
                                   placeholder="@lang('Note')">
                            <label>@lang('Note')</label>
                        </div>

                        <div class="new-address-map">
                            @error('map_location')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <div id="new-address-map"></div>
                            <a role="button" class="locationButton">
                                <img src="{{Ecommerce::theme()->asset('shop/images/checkout/location.png')}}">
                            </a>
                            <div class="locationInput-container">
                                <input type="hidden" value="{{ $addresse->map_location['lat'] }}" id="map-lat"
                                       name="map_location[lat]">
                                <input type="hidden" value="{{ $addresse->map_location['lng'] }}" id="map-lng"
                                       name="map_location[lng]">
                                <input type="text" value="{{ $addresse->map_location['address'] }}" id="locationInput"
                                       name="map_location[address]" class="locationInput">
                            </div>
                        </div>


                        <button type="submit" class="submit-btn">
                            @lang('Update')
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End Page Content -->


@endsection
@push('scripts')

    <script>
        $(function () {
            $(".country-select").on("change", function () {
                let country = $(this).val();
                let select = $(".city-select");
                select.empty().attr("disabled", "disabled");
                $.ajax({
                    url: route("admin.countries.country.cities", country),
                    success: function (data) {
                        select.html(data).removeAttr("disabled");
                    }
                });
            });
        });
    </script>



    <script
            src="https://maps.googleapis.com/maps/api/js?region=SA&language={{ \App::getLocale() }}&key={{ settings()->group('third-party')->get('google_map_key') }}&libraries=places">
    </script>
    <script>
        $(document).ready(function () {
            initMap()
        });

        function initMap() {
            const myLatLng = {
                lat: parseFloat("{{ $addresse->map_location['lat'] ?? '24.774255' }}"),
                lng: parseFloat("{{ $addresse->map_location['lat'] ?? '46.737586534' }}")
            };
            let map = new google.maps.Map(document.getElementById("new-address-map"), {
                center: myLatLng,
                zoom: 17,
            });
            let marker = new google.maps.Marker({
                position: map.getCenter(),
                draggable: true,
                map,
                icon: "{{Ecommerce::theme()->asset('shop/images/pin.png')}}"
            });
            const locationButton = document.getElementsByClassName('locationButton')[0];
            locationButton.addEventListener("click", () => {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        (position) => {
                            const pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude,
                            };
                            marker.setPosition(pos);
                            map.setCenter(pos);
                            updateInput(pos)
                        },
                    );
                }
            });

            google.maps.event.addListener(marker, "dragend", function (e) {
                updateInput(e.latLng)
            });
        }

        function updateInput(latLng) {
            var geocoder = new google.maps.Geocoder();
            $('#map-lat').val(latLng.lat);
            $('#map-lng').val(latLng.lng);
            geocoder.geocode({
                'latLng': latLng
            }, function (results) {
                $('#locationInput').val(results[0].formatted_address);
                document.getElementsByClassName('locationInput')[0].value = results[0].formatted_address;
            });
        }
    </script>
@endpush