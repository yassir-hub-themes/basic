<ul class="acc-list list-unstyled">
    <li class="acc-item">
        <a class="acc-link @if(Route::currentRouteName() == 'profile.index') active @endif" href="{{ route('profile.index') }}">
            <i class="fas fa-edit"></i>
            @lang('Account information')
        </a>
    </li>
    <li class="acc-item">
        <a class="acc-link @if(Route::currentRouteName() == 'password.index') active @endif" href="{{ route('password.index') }}">
            <i class="fas fa-key"></i>
            @lang('Change password')
        </a>
    </li>
    <li class="acc-item">
        <a class="acc-link @if(Route::currentRouteName() == 'addresses.index') active @endif" href="{{ route('addresses.index') }}">
            <i class="fas fa-address-book"></i>
            @lang('Address book')
        </a>
    </li>
    <li class="acc-item">
        <a class="acc-link @if(Route::currentRouteName() == 'orders.index') active @endif" href="{{ route('orders.index') }}">
            <i class="fas fa-file-alt"></i>
            @lang('My orders')
        </a>
    </li>
    <li class="acc-item">
        <a class="acc-link" href="{{ route('ecommerce.logout') }}">
            <i class="fas fa-sign-out-alt"></i>
            @lang('Logout')
        </a>
    </li>
</ul>